<form method="get" id="searchform" action="<?php echo home_url(); ?>/">
	<input type="search" class="sb-search-input" placeholder="<?php _e('Search', 'wpzoom') ?>"  name="s" id="s" />
    <input type="submit" id="searchsubmit" class="sb-search-submit" value="&#xe903;" alt="<?php _e('Search', 'wpzoom') ?>" />
</form>