<?php
/**
 * The template for displaying all pages.
 */

get_header(); ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <?php if ( has_post_thumbnail() ) { ?>
            <header class="entry-header<?php echo has_post_thumbnail() ? ' with-cover' : ''; ?>">

                <?php $entryCoverBackground = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'entry-cover' ); ?>

                <div class="entry-cover" style="background-image:url('<?php echo $entryCoverBackground[0]; ?>')">

                    <div class="inner-wrap">

                        <h1 class="entry-title">
                            <?php the_title(); ?>
                            <?php edit_post_link( __( 'Edit', 'wpzoom' ), '<small class="edit-link">', '</small>' ); ?>
                        </h1>

                    </div><!-- .inner-wrap -->

                </div><!-- .entry-cover -->

            </header><!-- .entry-header -->

        <?php } ?>

        <div class="inner-wrap">

            <main id="main" class="site-main<?php if ( option::is_on( 'post_thumb' ) && has_post_thumbnail() ) { ?> post-with-thumbnail<?php } ?>" role="main">

                <div class="post_wrap">

                    <?php get_template_part( 'content', 'page' ); ?>

                    <?php if (option::get('comments_page') == 'on') { ?>
                        <?php comments_template(); ?>
                    <?php } ?>

                </div>

            </main><!-- #main -->

    <?php endwhile; // end of the loop. ?>

    <div id="pp-sidebar" class="sidebar">
        <?php dynamic_sidebar('pp-sidebar'); ?>
    </div>

    <div class="clear"></div>

</div>

<?php get_footer(); ?>