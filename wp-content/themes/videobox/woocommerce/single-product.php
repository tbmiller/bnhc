<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header('shop'); ?>



 		<?php woocommerce_breadcrumb(); ?>


        <div class="inner-wrap full-width-page">



            <main id="main" class="site-main" role="main">


			<div class="entry-content">

				<?php do_action('woocommerce_output_content_wrapper'); ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php wc_get_template_part( 'content', 'single-product' ); ?>

					<?php endwhile; // end of the loop. ?>


				<div class="cleaner">&nbsp;</div>

			</div><!-- / .entry -->


    	</main><!-- /#main -->

        <div class="clear"></div>

    </div>

<?php get_footer('shop'); ?>