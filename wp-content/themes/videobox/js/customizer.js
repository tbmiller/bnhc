(function ($) {
	'use strict';

	/**
	* Document ready (jQuery)
	*/
	$(function () {
		var $orderfield = $('#customize-control-videobox_header-elements-order input'),
		    orderfieldVal = $.trim( $orderfield.val() ),
		    itemMenu = '<li id="videobox-header-elements-order-item-menu">' + wpzoomCustomizerI10n.menu + '</li>',
		    itemLogo = '<li id="videobox-header-elements-order-item-logo">' + wpzoomCustomizerI10n.logo + '</li>',
		    itemSearch = '<li id="videobox-header-elements-order-item-search">' + wpzoomCustomizerI10n.search + '</li>',
		    order = orderfieldVal.replace('M', itemMenu).replace('L', itemLogo).replace('S', itemSearch);

		$orderfield.hide();

		var $sortable = $('<ol class="videobox-header-elements-order-sortable jquery-sortable" tabindex="-1">' + order + '</ol>').insertAfter( $orderfield.parent('label') );

		$orderfield.parent('label').on('click', function(e){ e.preventDefault(); $sortable.focus(); });

		$sortable.sortable({
			axis: "y",
			containment: "parent",
			update: function(e,ui){
				var newval = '';

				$.each($sortable.sortable("toArray"), function(i,v){
					if ( v == 'videobox-header-elements-order-item-menu' )
						newval += 'M';
					else if ( v == 'videobox-header-elements-order-item-logo' )
						newval += 'L';
					else if ( v == 'videobox-header-elements-order-item-search' )
						newval += 'S';
				});

				$orderfield.val(newval).attr('value', newval).change();
			}
		});

		$('#customize-control-videobox_menu-visible input').on('change.wpzoom', function(){
			if ( !$(this).is(':checked') ) {
				$('#videobox-header-elements-order-item-menu').addClass('disabled');
				$('#customize-control-videobox_menu-type, #customize-control-videobox_menu-slide-dir').slideUp();
			} else {
				$('#videobox-header-elements-order-item-menu').removeClass('disabled');
				$('#customize-control-videobox_menu-type').slideDown();

				if ( $('#customize-control-videobox_menu-type select').val() == '1' )
					$('#customize-control-videobox_menu-slide-dir').slideDown();
			}
		});
		$('#customize-control-videobox_logo-visible input').on('change.wpzoom', function(){ if ( !$(this).is(':checked') ) $('#videobox-header-elements-order-item-logo').addClass('disabled'); else $('#videobox-header-elements-order-item-logo').removeClass('disabled'); });
		$('#customize-control-videobox_search-visible input').on('change.wpzoom', function(){ if ( !$(this).is(':checked') ) $('#videobox-header-elements-order-item-search').addClass('disabled'); else $('#videobox-header-elements-order-item-search').removeClass('disabled'); });

		$('#customize-control-videobox_menu-type select').on('change.wpzoom', function(){ if ( $(this).val() == '2' ) $('#customize-control-videobox_menu-slide-dir').slideUp(); else $('#customize-control-videobox_menu-slide-dir').slideDown(); });

		$('#customize-control-videobox_menu-visible input, #customize-control-videobox_logo-visible input, #customize-control-videobox_search-visible input, #customize-control-videobox_menu-type select').trigger('change.wpzoom');

		$('#customize-control-videobox_featured-image-visible input[type=checkbox]')
			.on('change.wpzoom', function(){
				if ( $(this).is(':checked') )
					$('#customize-control-videobox_featured-image-overlay-color, #customize-control-videobox_featured-image-overlay-opacity').removeClass('disabled');
				else
					$('#customize-control-videobox_featured-image-overlay-color, #customize-control-videobox_featured-image-overlay-opacity').addClass('disabled');
			})
			.trigger('change.wpzoom');

		$('#customize-control-videobox_featured-image-overlay-opacity input[type=range]')
			.wrap('<div class="overlay-opacity-wrap"></div>')
			.on('input.wpzoom', function(){
				$('#customize-control-videobox_featured-image-overlay-opacity-value').val( $(this).val() );
			});

		$('#customize-control-videobox_featured-image-overlay-opacity-value')
			.insertAfter('#customize-control-videobox_featured-image-overlay-opacity input[type=range]')
			.val( $('#customize-control-videobox_featured-image-overlay-opacity input[type=range]').val() )
			.on('input.wpzoom', function(){
				var val = $(this).val().replace( /[^0-9]/, '' );
				if ( val == '' ) val = 0;
				$(this).val( Math.min( 100, Math.max( 0, parseInt( val, 10 ) ) ) );
				$('#customize-control-videobox_featured-image-overlay-opacity input[type=range]')
					.val( $(this).val() )
					.attr( 'value', $(this).val() )
					.change();
			});
	});
})(jQuery);