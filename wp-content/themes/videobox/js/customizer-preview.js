(function ($) {
	'use strict';

	/**
	* Document ready (jQuery)
	*/
	$(function () {
		wp.customize( 'logo-visible', function( value ) {
			value.bind( function( to ) {
				if ( !to )
					$('.site-header > .inner-wrap > .navbar-brand').addClass('hidden');
				else
					$('.site-header > .inner-wrap > .navbar-brand').removeClass('hidden');
			} );
		} );

		wp.customize( 'menu-visible', function( value ) {
			value.bind( function( to ) {
				if ( !to ) {
					$('.site-header > .inner-wrap > .side-panel-btn, .site-header > .inner-wrap > .main-menu').addClass('hidden');
				} else {
					if ( wp.customize('menu-type').get() == 1 )
						$('.site-header > .inner-wrap > .side-panel-btn').removeClass('hidden');
					else
						$('.site-header > .inner-wrap > .main-menu').removeClass('hidden');
				}
			} );
		} );

		wp.customize( 'search-visible', function( value ) {
			value.bind( function( to ) {
				if ( !to )
					$('.site-header > .inner-wrap > .search-btn').addClass('hidden');
				else
					$('.site-header > .inner-wrap > .search-btn').removeClass('hidden');
			} );
		} );

		wp.customize( 'menu-type', function( value ) {
			value.bind( function( to ) {
				if ( to == 1 ) {
					$('.site-header > .inner-wrap > .side-panel-btn').removeClass('hidden');
					$('.site-header > .inner-wrap > .main-menu').addClass('hidden');
				} else {
					$('.site-header > .inner-wrap > .side-panel-btn').addClass('hidden');
					$('.site-header > .inner-wrap > .main-menu').removeClass('hidden');
				}
			} );
		} );

		wp.customize( 'menu-slide-dir', function( value ) {
			value.bind( function( to ) {
				if ( to == 2 )
					$('#pageslide').addClass('slide-from-right');
				else
					$('#pageslide').removeClass('slide-from-right');
			} );
		} );

		wp.customize( 'header-elements-order', function( value ) {
			value.bind( function( to ) {
				var order = array_flip( to.split('') );
				$('.site-header > .inner-wrap > .side-panel-btn, .site-header > .inner-wrap > .main-menu').removeClass(remove_order_class).addClass('order-' + (parseInt(order['M'], 10) + 1));
				$('.site-header > .inner-wrap > .navbar-brand').removeClass(remove_order_class).addClass('order-' + (parseInt(order['L'], 10) + 1));
				$('.site-header > .inner-wrap > .search-btn').removeClass(remove_order_class).addClass('order-' + (parseInt(order['S'], 10) + 1));
			} );
		} );

		wp.customize( 'featured-image-visible', function( value ) {
			value.bind( function( to ) {
				if ( !to ) {
					$('.entry-header .entry-cover').addClass('no-bg-img');
					$('.entry-header .entry-cover-overlay').addClass('hidden');
				} else {
					$('.entry-header .entry-cover').removeClass('no-bg-img');
					$('.entry-header .entry-cover-overlay').removeClass('hidden');
				}
			} );
		} );

		wp.customize( 'featured-image-overlay-color', function( value ) {
			value.bind( function( to ) {
				if ( valid_hex_color( to ) )
					$('.entry-header .entry-cover-overlay').css('background-color', to);
			} );
		} );

		wp.customize( 'featured-image-overlay-opacity', function( value ) {
			value.bind( function( to ) {
				var opacity = to.replace( /[^0-9]/, '' );
				if ( opacity == '' ) opacity = 0;
				opacity = Math.min( 100, Math.max( 0, parseInt( opacity, 10 ) ) ) / 100;
				$('.entry-header .entry-cover-overlay').css('opacity', opacity);
			} );
		} );
	});

	function array_flip( trans ) {
		var key, tmp_ar = {};

		for ( key in trans ) {
			if ( trans.hasOwnProperty( key ) ) {
				tmp_ar[trans[key]] = key;
			}
		}

		return tmp_ar;
	}

	function remove_order_class( index, css ) {
		return ( css.match(/(^|\s)order-[1-3]/ig) || [] ).join(' ');
	}

	function valid_hex_color( value ) {
		return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test( value );
	}
})(jQuery);