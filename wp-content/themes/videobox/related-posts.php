<?php $category = get_the_category();

 	$exclude = get_option( 'sticky_posts' );
	$exclude[] = $post->ID;

	$loop = new WP_Query(
		array(
			'post__not_in' => $exclude,
			'posts_per_page' => 3,
			'cat' => $category[0]->term_id
			) );

	$m = 0;
	if ( $loop->have_posts() ) :
?>
	<div class="widget related_posts">
		<h3 class="title"><?php _e('More in','wpzoom');?> <?php echo $category[0]->cat_name; ?>:</h3>

		<ul>

			<?php

			$exclude = get_option( 'sticky_posts' );
			$exclude[] = $post->ID;

			$loop = new WP_Query(
				array(
					'post__not_in' => $exclude,
					'posts_per_page' => 3,
					'cat' => $category[0]->term_id
					) );

			$m = 0;

			while ( $loop->have_posts() ) : $loop->the_post(); $m++;

                if ( !has_post_thumbnail() ) {
                      continue;
                } else {

        			?>

        			<li id="post-<?php the_ID(); ?>">

        				<div class="post-thumb"><?php the_post_thumbnail('loop'); ?></div>

                        <div class="item-content">
                            <h3 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
            				<div class="entry-meta">
                                <span class="entry-date"><?php echo get_the_date(); ?></span>
                                <?php if ( option::is_on( 'display_views' ) ) { ?><div class="meta_views"><?php
                                    $counter = get_post_meta( get_the_ID(), 'Views', true ) ? get_post_meta( get_the_ID(), 'Views', true ) : '0';
                                    printf( $counter );  ?> <?php _e('views', 'wpzoom'); ?></div><?php } ?>

                            </div>
                        </div>

        			</li><!-- end #post-<?php the_ID(); ?> -->

                <?php } ?>

			<?php endwhile; ?>

		</ul><!-- end .posts -->


	</div><!-- /.related_posts -->
<?php endif; ?>
<?php wp_reset_query(); ?>