<article id="post-<?php the_ID(); ?>" data-comments="<?php echo get_comments_number(); ?>" <?php post_class( get_comments_number() > 0 ? 'has-comments' : null ); ?>>

    <?php if ( option::is_on('display_thumb') ) {
          if ( has_post_thumbnail() ) : ?>
            <div class="post-thumb"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">

                <?php if (option::get('thumb_ratio') == 'Landscape') {
                    $image_size = "loop";
                } else {
                    $image_size = "loop-portrait";
                } ?>
                    <?php the_post_thumbnail($image_size); ?>

            </a></div>
        <?php endif;
    } ?>

    <section class="entry-body">

        <div class="entry-meta">
            <?php if ( option::is_on( 'display_category' ) ) printf( '<span class="cat-links">%s</span>', get_the_category_list( ', ' ) ); ?>
            <?php if ( option::is_on( 'display_views' ) ) { ?><div class="meta_views"><?php
            $counter = get_post_meta( get_the_ID(), 'Views', true ) ? get_post_meta( get_the_ID(), 'Views', true ) : '0';
            printf( $counter );  ?> <?php _e('views', 'wpzoom'); ?></div><?php } ?>
         </div>

        <?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>

        <div class="entry-meta">
            <?php if ( option::is_on( 'display_date' ) )  printf( '<span class="entry-date"><time class="entry-date" datetime="%1$s">%2$s</time></span>', esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ) ); ?>
            <?php if ( option::is_on( 'display_author' ) ) { printf( '<span class="entry-author">%s ', __( 'by', 'wpzoom' ) ); the_author_posts_link(); print('</span>'); } ?>
            <?php if ( option::is_on( 'display_comments' ) ) { ?><span class="comments-link"><?php comments_popup_link( __('0 comments', 'wpzoom'), __('1 comment', 'wpzoom'), __('% comments', 'wpzoom'), '', __('Comments are Disabled', 'wpzoom')); ?></span><?php } ?>
            <?php edit_post_link( __( 'Edit', 'wpzoom' ), '<span class="edit-link">', '</span>' ); ?>
        </div>

        <?php if ( ( is_archive() || is_search() ) || ( is_home() &&  option::is_on('display_excerpt')  ) ) { the_excerpt(); } ?>


    </section>

    <div class="clearfix"></div>

</article><!-- #post-<?php the_ID(); ?> -->