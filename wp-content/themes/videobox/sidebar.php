<?php
/**
 * The sidebar.
 */
?>

<div id="sidebar" class="site-sidebar">

    <?php dynamic_sidebar('sidebar'); ?>

</div>
