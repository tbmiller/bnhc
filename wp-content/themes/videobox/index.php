<?php
/**
 * The main template file.
 */

get_header(); ?>

<?php if ( option::is_on( 'featured_posts_show' ) && is_front_page() && $paged < 2) : ?>

	<?php get_template_part( 'wpzoom-slider' ); ?>

<?php endif; ?>

<main id="main" class="site-main" role="main">

	<?php if ( is_active_sidebar( 'homepage' ) && is_front_page() && $paged < 2  ) : ?>

		<section class="site-widgetized-section home-widgetized-sections home-widgetized-section-upper">

			<div class="inner-wrap">

				<div class="widgets clearfix">

					<?php dynamic_sidebar( 'homepage' ); ?>

				</div>

			</div>

		</section><!-- .site-widgetized-section -->

	<?php endif; ?>

	<div class="inner-wrap">

		<h2 class="section-title sort">
			<?php $popular = isset( $_GET['sort'] ) && $_GET['sort'] == 'popular'; ?>
			<span class="sort-latest<?php echo !$popular ? ' selected' : ''; ?>" data-url="<?php echo esc_url( remove_query_arg( 'sort' ) ); ?>"><?php _e( 'Latest', 'wpzoom' ); ?></span>
			<?php if(option::is_on('display_popular') ) { ?><em class="sort-separator">/</em>
			<span class="sort-popular<?php echo $popular ? ' selected' : ''; ?>" data-url="<?php echo esc_url( add_query_arg( 'sort', 'popular' ) ); ?>"><?php _e( 'Popular', 'wpzoom' ); ?></span><?php } ?>
		</h2>


		<div id="recent-posts-wrap">

			<div>

				<?php if ( have_posts() ) : ?>

					<section class="recent-posts" id="recent-posts">

						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'content', get_post_format() ); ?>

						<?php endwhile; ?>

					</section>

					<?php get_template_part( 'pagination' ); ?>

				<?php else: ?>

					<?php get_template_part( 'content', 'none' ); ?>

				<?php endif; ?>

			</div>

		</div>


		<?php if ( is_active_sidebar( 'homepage-lower' ) && is_front_page() && $paged < 2  ) : ?>

			<section class="site-widgetized-section home-widgetized-sections home-widgetized-section-lower">

				<div class="widgets clearfix">

					<?php dynamic_sidebar( 'homepage-lower' ); ?>

				</div>

			</section><!-- .site-widgetized-section -->

		<?php endif; ?>

	</div>

</main><!-- .site-main -->

<?php get_footer();