<?php get_header(); ?>

<main id="main" class="site-main" role="main">

    <header class="entry-header entry-header-404">

        <div class="entry-info">

            <div class="inner-wrap">

                <h1 class="entry-title"><?php _e( 'Error 404', 'wpzoom' ); ?></h1>

            </div>

        </div>

    </header><!-- .entry-header -->

    <div class="inner-wrap">

	    <div class="post_wrap">

	        <?php get_template_part( 'content', 'none' ); ?>

	    </div><!-- .post_wrap -->

    </div>

</main><!-- .site-main -->

<?php get_footer(); ?>
