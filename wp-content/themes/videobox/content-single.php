<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-info">

        <div class="entry-meta">
            <?php if ( option::is_on( 'post_views' ) ) { ?><div class="meta_views"><?php
            $counter = get_post_meta( get_the_ID(), 'Views', true ) ? get_post_meta( get_the_ID(), 'Views', true ) : '0';
            printf( $counter );  ?> <?php _e('views', 'wpzoom'); ?></div><?php } ?>
            <?php if ( option::is_on( 'post_category' ) ) printf( '<span class="cat-links">%s</span>', get_the_category_list( ', ' ) ); ?>
         </div>

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta second-entry-meta">
            <?php if ( option::is_on( 'post_date' ) )  printf( '<span class="entry-date"><time class="entry-date" datetime="%1$s">%2$s</time></span>', esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ) ); ?>
            <?php if ( option::is_on( 'post_author' ) ) { printf( '<span class="entry-author">%s ', __( 'By', 'wpzoom' ) ); the_author_posts_link(); print('</span>'); } ?>
            <?php if ( option::is_on( 'post_comments_count' ) ) { ?><span class="comments-link"><?php comments_popup_link( __('0 comments', 'wpzoom'), __('1 comment', 'wpzoom'), __('% comments', 'wpzoom'), '', __('Comments are Disabled', 'wpzoom')); ?></span><?php } ?>
            <?php edit_post_link( __( 'Edit', 'wpzoom' ), '<span class="edit-link">', '</span>' ); ?>

        </div>

	</div>


	<div class="post_wrap">

		<div class="entry-content">

			<?php the_content(); ?>

			<div class="clear"></div>

			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'wpzoom' ),
					'after'  => '</div>',
				) );
			?>

		</div><!-- .entry-content -->


		<?php if ( option::is_on( 'post_tags' ) ) : ?>

		   <?php the_tags( '<div class="tag_list">', '', '</div>' ); ?>

	   <?php endif; ?>

	   <div class="clear"></div>

	</div><!-- .post-wrap -->

</article><!-- #post-->