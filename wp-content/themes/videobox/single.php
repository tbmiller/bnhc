<?php
/**
 * The Template for displaying all single posts.
 */

get_header(); ?>

<?php $show_media = get_post_meta($post->ID, 'wpzoom_show_media', true); ?>
<?php $template = get_post_meta($post->ID, 'wpzoom_post_template', true); ?>


    <?php while ( have_posts() ) : the_post(); ?>

        <?php $entryCoverBackground = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'entry-cover' ); ?>

        <?php if (!$show_media ) { ?>

            <?php
            $has_video = strlen( $video = get_post_meta( $post->ID, 'wpzoom_post_embed_code', true ) ) > 1;
            $show_feat_img = get_theme_mod( 'featured-image-visible', videobox_get_default( 'featured-image-visible' ) );
            ?>

            <header class="entry-header">

                <div class="entry-cover-outer">

                    <div class="entry-cover<?php echo $has_video ? ' has-video' : ''; echo $has_video && !$show_feat_img ? ' no-bg-img' : ''; ?>" style="background-image:url('<?php echo $entryCoverBackground[0]; ?>')"></div>

                    <?php if ( $has_video ) { ?><div class="entry-cover-overlay<?php echo !$show_feat_img ? ' hidden' : ''; ?>"></div><?php } ?>

                    <?php
                    if ( $has_video ) { // Embedding video
                        printf( '<div class="inner-wrap"><div class="video_cover">%s</div></div>', embed_fix( $video, 1200, 675 ) ); // add wmode=transparent to iframe/embed
                    }
                    ?>

                </div>

            </header><!-- .entry-header -->
        <?php } ?>

        <div class="inner-wrap">

            <?php if ($template == 'full') { echo '<div class="full-width-page">'; } ?>

            <main id="main" class="site-main<?php if ( has_post_thumbnail() && !$show_media ) { ?> post-with-thumbnail<?php } ?>" role="main">

                <?php get_template_part( 'content', 'single' ); ?>

                <?php if ( option::is_on( 'post_author_box' ) ) { ?>

                    <footer class="entry-footer">

                        <div class="post_author">

                            <?php echo get_avatar( get_the_author_meta( 'ID' ) , 100 ); ?>

                            <div class="author-description">

                                <h3 class="author-title author"><?php the_author_posts_link(); ?></h3>

                                <p class="author-bio">
                                    <?php the_author_meta( 'description' ); ?>
                                </p>

                                <div class="author_links">

                                    <?php if ( get_the_author_meta( 'facebook_url' ) ) { ?><a class="author_facebook" href="<?php the_author_meta( 'facebook_url' ); ?>" title="Facebook Profile" target="_blank">Facebook</a><?php } ?>


                                    <?php if ( get_the_author_meta( 'twitter' ) ) { ?><a class="author_twitter" href="https://twitter.com/<?php the_author_meta( 'twitter' ); ?>" title="Follow <?php the_author_meta( 'display_name' ); ?> on Twitter" target="_blank">Twitter</a><?php } ?>


                                    <?php if ( get_the_author_meta( 'instagram_url' ) ) { ?><a class="author_instagram" href="https://instagram.com/<?php the_author_meta( 'instagram_url' ); ?>" title="Instagram" target="_blank">Instagram</a><?php } ?>

                                </div>

                            </div>

                            <div class="clear"></div>

                        </div>

                    </footer><!-- .entry-footer -->

                <?php } ?>

               <?php if ( is_active_sidebar( 'sidebar-post' ) ) : ?>

                   <section class="section-single">

                       <?php dynamic_sidebar( 'sidebar-post' ); ?>

                   </section><!-- .site-widgetized-section -->

               <?php endif; ?>

                <?php if (option::get('post_comments') == 'on') : ?>

                    <?php comments_template(); ?>

                <?php endif; ?>


            <?php endwhile; // end of the loop. ?>

            </main><!-- #main -->


        <?php if ($template != 'full') { ?>

            <div id="pp-sidebar" class="sidebar">

                <?php if (option::is_on('post_related')) {

                    if (is_singular( 'post' )) {
                        get_template_part('related-posts');
                    }
                } ?>

                <?php dynamic_sidebar('pp-sidebar'); ?>
            </div>

        <?php } ?>

        <div class="clear"></div>

        <?php if ($template == 'full') { echo '</div>'; } ?>

    </div>

<?php get_footer(); ?>