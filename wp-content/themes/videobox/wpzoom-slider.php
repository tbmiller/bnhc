<?php
$featured = new WP_Query( array(
    'showposts'    => option::get( 'slideshow_posts' ),
    'post__not_in' => get_option( 'sticky_posts' ),
    'meta_key'     => 'wpzoom_is_featured',
    'meta_value'   => 1,
) );

?>

<div id="slider">

    <div class="inner-wrap">

	<?php if ( $featured->have_posts() ) : ?>

		<ul class="slides clearfix">

			<?php $count = 0; while ( $featured->have_posts() ) : $featured->the_post(); ?>

                <?php
                $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'slider');


                $style = ' style="background-image:url(\'' . $large_image_url[0] . '\')"';

                ?>

                <li class="slide">


                    <div class="slide-overlay">

                        <div class="slide-header">

                           <div class="entry-meta">
                                <?php if ( option::is_on( 'slider_category' ) ) {
                                    $categories = (array)wp_get_post_terms( get_the_ID(), 'category' );
                                    $catnum = count( $categories );

                                    if ( $catnum > 0 ) {
                                        $i = 1;

                                        ?><span class="cat-links">
                                            <?php foreach ( $categories as $category ) {
                                                $color = esc_attr( preg_replace( '/[^0-9a-fA-F]/', '', trim( get_term_meta( $category->term_id, 'wpzoom_category_color', true ) ) ) );
                                                printf( '<a href="%s" rel="category"%s>%s</a>%s', esc_url( get_category_link( $category->term_id ) ), ( !empty( $color ) ? " style='color:#$color'" : '' ), $category->name, ( $i < $catnum ? ', ' : '' ) );
                                                $i++;
                                            } ?>
                                        </span><?php
                                    }
                                } ?>
                             </div>

                            <?php the_title( sprintf( '<h3 class="entry-title"><a href="%s">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>

                            <div class="entry-meta">

                                <?php if ( option::is_on( 'slider_views' ) ) { ?><span class="meta_views"><?php
                                    $counter = get_post_meta( get_the_ID(), 'Views', true ) ? get_post_meta( get_the_ID(), 'Views', true ) : '0';
                                    printf( $counter );  ?> <?php _e('views', 'wpzoom'); ?></span><?php } ?>

                                <?php if ( option::is_on( 'slider_date' ) )     printf( '<span class="entry-date"><time class="entry-date" datetime="%1$s">%2$s</time></span>', esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ) ); ?>
                                 <?php if ( option::is_on( 'slider_comments' ) ) { ?><span class="comments-link"><?php comments_popup_link( __('0 comments', 'wpzoom'), __('1 comment', 'wpzoom'), __('% comments', 'wpzoom'), '', __('Comments are Disabled', 'wpzoom')); ?></span><?php } ?>
                             </div>
                        </div>

                    </div>

                    <div class="slide-background" <?php echo $style; ?>>
                    </div>
                </li>
            <?php $count++; endwhile; ?>

		</ul>

        <?php if ( option::is_on( 'slider_count' ) ) { ?><div class="slides-count">
            <div class="inner-wrap">
                <p><span class="current-slide-num">1</span> <span class="slides-count-sep">|</span> <span class="all-slides-num"><?php echo $count; ?></span></p>
            </div>
        </div><?php } ?>

	<?php else: ?>

		<div class="empty-slider">
			<p><strong><?php _e( 'You are now ready to set-up your Slideshow content.', 'wpzoom' ); ?></strong></p>

			<p>
				<?php
				printf(
					__( 'For more information about adding posts to the slider, please <a href="%1$s">read the documentation</a>', 'wpzoom' ),
					'http://www.wpzoom.com/documentation/videobox/'
				);
				?>
			</p>
		</div>

	<?php endif; ?>

    </div><!-- /.inner-wrap -->

</div>