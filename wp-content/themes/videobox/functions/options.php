<?php return array(


/* Theme Admin Menu */
"menu" => array(
    array("id"    => "1",
          "name"  => "General"),

    array("id"    => "2",
          "name"  => "Homepage"),

),

/* Theme Admin Options */
"id1" => array(
    array("type"  => "preheader",
          "name"  => "Theme Settings"),

    array("name"  => "Custom Feed URL",
          "desc"  => "Example: <strong>http://feeds.feedburner.com/wpzoom</strong>",
          "id"    => "misc_feedburner",
          "std"   => "",
          "type"  => "text"),

	array("name"  => "Enable comments for static pages",
          "id"    => "comments_page",
          "std"   => "off",
          "type"  => "checkbox"),



    array(
            "type" => "preheader",
            "name" => "Global Posts Options"
        ),


        array(
            "name" => "Display Featured Image at the Top",
            "id" => "display_thumb",
            "std" => "on",
            "type" => "checkbox"
        ),


        array("name"  => "Featured Image Aspect Ratio",
            "id"    => "thumb_ratio",
            "options" => array('Landscape', 'Portrait'),
            "std"   => "Landscape",
            "type"  => "select"),


        array(
            "name" => "Display Excerpt in Recent Posts",
            "id" => "display_excerpt",
            "std" => "on",
            "type" => "checkbox"
        ),

        array(
            "name" => "Excerpt length",
            "desc" => "Default: <strong>45</strong> (words)",
            "id" => "excerpt_length",
            "std" => "45",
            "type" => "text"
        ),

        array(
            "type" => "startsub",
            "name" => "Post Meta",
        ),

            array(
                "name" => "Display Category",
                "id" => "display_category",
                "std" => "on",
                "type" => "checkbox"
            ),

            array(
                "name" => "Display Views Count",
                "id" => "display_views",
                "std" => "on",
                "type" => "checkbox"
            ),
            array(
                "name" => "Display Date/Time",
                "desc" => "<strong>Date/Time format</strong> can be changed <a href='options-general.php' target='_blank'>here</a>.",
                "id" => "display_date",
                "std" => "on",
                "type" => "checkbox"
            ),

            array(
                "name" => "Display Author",
                "id" => "display_author",
                "std" => "on",
                "type" => "checkbox"
            ),

            array(
                "name" => "Display Comments Count",
                "id" => "display_comments",
                "std" => "on",
                "type" => "checkbox"
            ),

        array(
            "type" => "endsub"
        ),

        array(
            "type" => "startsub",
            "name" => "Infinite Scroll",
        ),

            array(
                "desc" => sprintf('This feature depends on <a href="http://jetpack.me" target="_blank">JetPack</a>, please install it first and then <a href="http://jetpack.me/support/activate-and-deactivate-modules/" target="_blank">activate Infinite Scroll module</a>. <br>Then navigate to %1$s to select a trigger for infinite scroll.', sprintf('<a href="%1$s" target="_blank">Reading Settings</a>', esc_url(admin_url('options-reading.php#infinite-scroll-options')))),
                "type" => "paragraph",
            ),

            array(
                "name" => "Load More Button Text",
                "desc" => "Used only when Scroll Infinitely is disabled in Reading Settings.",
                "id"   => "infinite_scroll_handle_text",
                "type" => "text",
                "std"  => "Load More..."
            ),

        array(
            "type" => "endsub"
        ),


    array(
        "type" => "preheader",
        "name" => "Single Post Options"
    ),

        array(
            "type" => "startsub",
            "name" => "Post Meta",
        ),

            array(
                "name" => "Display Category",
                "id" => "post_category",
                "std" => "on",
                "type" => "checkbox"
            ),

            array(
                "name" => "Display Views Count",
                "id" => "post_views",
                "std" => "on",
                "type" => "checkbox"
            ),

            array(
                "name" => "Display Date/Time",
                "desc" => "<strong>Date/Time format</strong> can be changed <a href='options-general.php' target='_blank'>here</a>.",
                "id" => "post_date",
                "std" => "on",
                "type" => "checkbox"
            ),

            array(
                "name" => "Display Author Name",
                "desc" => "You can edit your profile on this <a href='profile.php' target='_blank'>page</a>.",
                "id" => "post_author",
                "std" => "on",
                "type" => "checkbox"
            ),

            array(
                "name" => "Display Comments Count",
                "id" => "post_comments_count",
                "std" => "on",
                "type" => "checkbox"
            ),

        array(
            "type" => "endsub"
        ),


        array("name"  => "Display Related Posts in Sidebar",
             "desc" => "A widget showing latest posts from the same category as current post",
             "id"    => "post_related",
             "std"   => "on",
             "type"  => "checkbox"),

        array(
            "name" => "Display Tags",
            "id" => "post_tags",
            "std" => "on",
            "type" => "checkbox"
        ),

        array(
            "name" => "Display Author Profile Box",
            "desc" => "You can edit your profile on this <a href='profile.php' target='_blank'>page</a>.",
            "id" => "post_author_box",
            "std" => "on",
            "type" => "checkbox"
        ),

        array(
            "name" => "Display Comments",
            "id" => "post_comments",
            "std" => "on",
            "type" => "checkbox"
        ),

    array(
        "type" => "preheader",
        "name" => "Archive Page Options"
    ),

        array(
            "name" => "Display Archive Header Background Image",
            "id" => "archive_header_background",
            "std" => "on",
            "type" => "checkbox"
        ),

    ),


"id2" => array(

  array("type"  => "preheader",
        "name"  => "Recent Posts"),

  array("name"  => "Exclude categories",
        "desc"  => "Choose the categories which should be excluded from the main Loop on the homepage.<br/><em>Press CTRL or CMD key to select/deselect multiple categories </em>",
        "id"    => "recent_part_exclude",
        "std"   => "",
        "type"  => "select-category-multi"),

  array("name"  => "Hide Featured Posts in Recent Posts?",
        "desc"  => "You can use this option if you want to hide posts which are featured in the slider on front page.",
        "id"    => "hide_featured",
        "std"   => "off",
        "type"  => "checkbox"),

    array(
        "type" => "startsub",
        "name" => "Popular Posts",
    ),


        array(
            "name" => "Display Popular Posts",
            "id" => "display_popular",
            "std" => "on",
            "type" => "checkbox"
        ),

        array("name"    => "Method",
              "desc"    => "How popular posts should be determined. (i.e. by number of comments or number or views)",
              "id"      => "popular_posts_method",
              "options" => array('Number of Views', 'Number of Comments'),
              "std"     => "Number of Views",
              "type"    => "select"),

        array("name"    => "Time Period",
              "desc"    => "How long of a period the popular posts on the homepage are displayed from. (e.g. Show the most popular posts from the last week)",
              "id"      => "popular_posts_time_period",
              "options" => array('All Time', 'This Year', 'This Month', 'This Week'),
              "std"     => "All Time",
              "type"    => "select"),

    array(
        "type" => "endsub"
    ),

    array("type"  => "preheader",
          "name"  => "Homepage Slideshow"),

    array("name"  => "Display Slideshow on homepage?",
          "desc"  => "Do you want to show a featured slider on the homepage? To add posts in slider just check the option <strong>Featured in Homepage Slider</strong> in the post.",
          "id"    => "featured_posts_show",
          "std"   => "on",
          "type"  => "checkbox"),

    array("name"  => "Autoplay Slideshow?",
          "desc"  => "Do you want to auto-scroll the slides?",
          "id"    => "slideshow_auto",
          "std"   => "off",
          "type"  => "checkbox",
          "js"    => true),

    array("name"  => "Slider Autoplay Interval",
          "desc"  => "Select the interval (in miliseconds) at which the Slider should change posts (<strong>if autoplay is enabled</strong>). Default: 3000 (3 seconds).",
          "id"    => "slideshow_speed",
          "std"   => "3000",
          "type"  => "text",
          "js"    => true),

    array("name"  => "Number of Posts in Slider",
          "desc"  => "How many posts should appear in  Slider on the homepage? Default: 5.",
          "id"    => "slideshow_posts",
          "std"   => "5",
          "type"  => "text"),

    array(
        "name" => "Display Category",
         "id" => "slider_category",
        "std" => "on",
        "type" => "checkbox"
    ),

    array(
        "name" => "Display Views Count",
        "id" => "slider_views",
        "std" => "on",
        "type" => "checkbox"
    ),

    array(
        "name" => "Display Date/Time",
        "desc" => "<strong>Date/Time format</strong> can be changed <a href='options-general.php' target='_blank'>here</a>.",
        "id" => "slider_date",
        "std" => "on",
        "type" => "checkbox"
    ),

    array(
        "name" => "Display Comments Count",
        "id" => "slider_comments",
        "std" => "on",
        "type" => "checkbox"
    ),

    array(
        "name" => "Display Number of Posts (1 of 5)",
         "id" => "slider_count",
        "std" => "on",
        "type" => "checkbox"
    ),



)

/* end return */);