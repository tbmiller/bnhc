<?php

function videobox_customizer_define_color_scheme_sections( $sections ) {
    $panel           = WPZOOM::$theme_raw_name . '_color-scheme';
    $colors_sections = array();

    $colors_sections['color'] = array(
        'panel'   => $panel,
        'title'   => __( 'General', 'wpzoom' ),
        'options' => array(

            'color-body-text' => array(
                'setting' => array(
                    'sanitize_callback' => 'maybe_hash_hex_color',
                    'transport'  => 'postMessage'
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Body Text', 'wpzoom' ),
                ),
            ),

            'color-logo' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Logo', 'wpzoom' ),
                ),
            ),

            'color-logo-hover' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Logo Hover', 'wpzoom' ),
                ),
            ),

            'color-link' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Link', 'wpzoom' ),
                ),
            ),

            'color-link-hover' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Link Hover', 'wpzoom' ),
                ),
            ),

            'color-hamburger' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Menu Icon (hamburger)', 'wpzoom' ),
                ),
            ),

            'color-hamburger-hover' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Menu Icon (hamburger) Hover', 'wpzoom' ),
                ),
            ),

            'color-search' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Search Icon', 'wpzoom' ),
                ),
            ),

            'color-search-hover' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Search Icon Hover', 'wpzoom' ),
                ),
            ),

        )
    );

    $colors_sections['color-main-menu'] = array(
        'panel'   => $panel,
        'title'   => __( 'Main Menu', 'wpzoom' ),
        'options' => array(

            /**
             * Main Menu
             */

            'color-menu-link' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Menu Item', 'wpzoom' ),
                ),
            ),

            'color-menu-link-hover' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Menu Item Hover', 'wpzoom' ),
                ),
            ),

            'color-menu-link-current' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Menu Current Item', 'wpzoom' ),
                ),
            ),

        )
    );

    $colors_sections['color-slider'] = array(
        'panel'   => $panel,
        'title'   => __( 'Homepage Slider', 'wpzoom' ),
        'options' => array(

            /**
             * Slider
             */
            'color-slider-background' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Slider Area Background', 'wpzoom' ),
                ),
            ),

        )
    );

    $colors_sections['color-posts'] = array(
        'panel'   => $panel,
        'title'   => __( 'Recent Posts', 'wpzoom' ),
        'options' => array(

            /**
             * Post
             */

            'color-post-title' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Post Title', 'wpzoom' ),
                ),
            ),

            'color-post-title-hover' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Post Title Hover', 'wpzoom' ),
                ),
            ),

            'color-post-meta' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Post Meta', 'wpzoom' ),
                ),
            ),

            'color-post-meta-link' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Post Meta Link', 'wpzoom' ),
                ),
            ),

            'color-post-meta-link-hover' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Post Meta Link Hover', 'wpzoom' ),
                ),
            ),

        )
    );


    $colors_sections['color-widgets'] = array(
        'panel'   => $panel,
        'title'   => __( 'Widgets', 'wpzoom' ),
        'options' => array(

            /**
             * Widgets
             */

            'color-widget-title' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Widget Title', 'wpzoom' ),
                ),
            ),

            'color-widget-title-footer' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Widget Title in Footer', 'wpzoom' ),
                ),
            ),

        )
    );

    $colors_sections['color-footer'] = array(
        'panel'   => $panel,
        'title'   => __( 'Footer', 'wpzoom' ),
        'options' => array(

            /**
             * Footer
             */

            'color-footer-background' => array(
                'setting' => array(
                    'transport'  => 'postMessage',
                    'sanitize_callback' => 'maybe_hash_hex_color',
                ),
                'control' => array(
                    'control_type' => 'WP_Customize_Color_Control',
                    'label'        => __( 'Footer Area Background', 'wpzoom' ),
                ),
            ),


        )
    );

    return array_merge( $sections, $colors_sections );
}

add_filter( 'zoom_customizer_sections', 'videobox_customizer_define_color_scheme_sections' );
