<?php

function videobox_customizer_define_single_sections( $sections ) {
    $panel = WPZOOM::$theme_raw_name . '_single';

    $new_sections = array();

    $new_sections['single'] = array(
        'panel'   => $panel,
        'title'   => __( 'Featured Video', 'wpzoom' ),
        'options' => array(
            'featured-image-visible' => array(
                'setting' => array(
                    'sanitize_callback' => 'absint',
                    'transport' => 'postMessage'
                ),
                'control' => array(
                    'label' => __( 'Show Featured Image', 'wpzoom' ),
                    'description' => __( 'Whether the featured image should be displayed behind the featured video.', 'wpzoom' ),
                    'type'  => 'checkbox'
                )
            ),
            'featured-image-overlay-color' => array(
                'setting' => array(
                    'sanitize_callback' => 'maybe_hash_hex_color',
                    'transport' => 'postMessage'
                ),
                'control' => array(
                    'label' => __( 'Overlay Color', 'wpzoom' ),
                    'description' => __( 'The color of the overlay placed on top of the featured image behind the featured video.', 'wpzoom' ),
                    'control_type'  => 'WP_Customize_Color_Control'
                )
            ),
            'featured-image-overlay-opacity' => array(
                'setting' => array(
                    'sanitize_callback' => 'absint',
                    'transport' => 'postMessage'
                ),
                'control' => array(
                    'label' => __( 'Overlay Opacity', 'wpzoom' ),
                    'description' => __( 'The opacity of the color of the overlay placed on top of the featured image behind the featured video.', 'wpzoom' ) . '<input type="text" id="customize-control-videobox_featured-image-overlay-opacity-value" size="3" class="code" />',
                    'type' => 'range',
                    'input_attrs' => array(
                        'min'   => 0,
                        'max'   => 100,
                        'step'  => 1
                    )
                )
            )
        )
    );

    return array_merge( $sections, $new_sections );
}

add_filter( 'zoom_customizer_sections', 'videobox_customizer_define_single_sections' );
