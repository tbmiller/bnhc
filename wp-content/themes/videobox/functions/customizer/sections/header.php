<?php

function videobox_customizer_define_header_sections( $sections ) {
    $panel           = WPZOOM::$theme_raw_name . '_header';
    $header_sections = array();

    $theme_prefix = WPZOOM::$theme_raw_name . '_';

    /**
     * Header Layout
     */
    $header_sections['layout'] = array(
        'title'   => __( 'Header', 'wpzoom' ),
        'options' => array(
            'logo-visible' => array(
                'setting' => array(
                    'sanitize_callback' => 'absint',
                    'transport' => 'postMessage'
                ),
                'control' => array(
                    'label' => __( 'Logo Visible', 'wpzoom' ),
                    'type'  => 'checkbox'
                )
            ),
            'menu-visible' => array(
                'setting' => array(
                    'sanitize_callback' => 'absint',
                    'transport' => 'postMessage'
                ),
                'control' => array(
                    'label' => __( 'Menu Visible', 'wpzoom' ),
                    'type'  => 'checkbox'
                )
            ),
            'search-visible' => array(
                'setting' => array(
                    'sanitize_callback' => 'absint',
                    'transport' => 'postMessage'
                ),
                'control' => array(
                    'label' => __( 'Search Field Visible', 'wpzoom' ),
                    'type'  => 'checkbox'
                )
            ),
            'header-elements-order' => array(
                'setting' => array(
                    'sanitize_callback' => 'esc_attr',
                    'transport' => 'postMessage'
                ),
                'control' => array(
                    'label' => __( 'Order', 'wpzoom' ),
                    'description' => __( 'Drag to change order', 'wpzoom' ),
                    'type'  => 'text'
                )
            ),
            'menu-type' => array(
                'setting' => array(
                    'sanitize_callback' => 'absint',
                    'transport' => 'postMessage'
                ),
                'control' => array(
                    'label' => __( 'Menu Type', 'wpzoom' ),
                    'type'  => 'select',
                    'choices' => array(
                        1 => __( 'Icon w/ Slideout Menu', 'wpzoom' ),
                        2 => __( 'Full Menu', 'wpzoom' ),
                    )
                )
            ),
            'menu-slide-dir' => array(
                'setting' => array(
                    'sanitize_callback' => 'absint',
                    'transport' => 'postMessage'
                ),
                'control' => array(
                    'label' => __( 'Menu Slide Direction', 'wpzoom' ),
                    'type'  => 'select',
                    'choices' => array(
                        1 => __( 'From Left', 'wpzoom' ),
                        2 => __( 'From Right', 'wpzoom' ),
                    )
                )
            )
        )
    );

    return array_merge( $sections, $header_sections );
}

add_filter( 'zoom_customizer_sections', 'videobox_customizer_define_header_sections' );
