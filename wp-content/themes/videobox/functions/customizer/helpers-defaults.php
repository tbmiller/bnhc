<?php

function videobox_option_defaults() {
    $defaults = array(
        /**
         * General
         */
        // Site Title & Tagline
        'hide-tagline'                        => 0,
        // Header
        'logo-visible'                        => 1,
        'menu-visible'                        => 1,
        'search-visible'                      => 1,
        'menu-type'                           => 1,
        'menu-slide-dir'                      => 1,
        'header-elements-order'               => 'MLS',
        // Logo
        'logo'                                => '',
        'logo-retina-ready'                   => 0,
        'logo-favicon'                        => 0,

        /**
         * Typography
         */
        // Body
        'font-family-site-body'               => 'Libre Franklin',
        'font-size-site-body'                 => 16,
        'font-weight-site-body' => 'normal',
        'font-style-site-body' => 'normal',

        // Paragraph
        'font-family-site-paragraph'          => 'Libre Franklin',
        'font-size-site-paragraph'            => 16,
        'font-weight-site-paragraph' => 'normal',
        'font-style-site-paragraph' => 'normal',

        // Site Title & Tag Line
        'font-family-site-title'              => 'Libre Franklin',
        'font-size-site-title'                => 36,
        'font-weight-site-title' => 'bold',
        'font-style-site-title' => 'normal',
        'font-transform-site-title' => 'uppercase',

        // Main Navigation
        'font-family-nav'                     => 'Libre Franklin',
        'font-size-nav' => 16,
        'font-weight-nav' => 'normal',
        'font-style-nav' => 'normal',
        'font-transform-nav' => 'none',

        // Slider Title
        'font-family-slider-title'            => 'Libre Franklin',
        'font-size-slider-title'              => 46,
        'font-weight-slider-title' => 'bold',
        'font-style-slider-title' => 'normal',
        'font-transform-slider-title' => 'none',

        // Widgets
        'font-family-widgets'                 => 'Libre Franklin',
        'font-size-widgets'                   => 21,
        'font-weight-widgets' => 'bold',
        'font-style-widgets' => 'normal',
        'font-transform-widgets' => 'none',

        // Post Title
        'font-family-post-title'              => 'Libre Franklin',
        'font-size-post-title'                => 22,
        'font-weight-post-title' => 'bold',
        'font-style-post-title' => 'normal',
        'font-transform-post-title' => 'none',

        // Single Post Title
        'font-family-single-post-title'       => 'Libre Franklin',
        'font-size-single-post-title'         => 46,
        'font-weight-single-post-title' => 'bold',
        'font-style-single-post-title' => 'normal',
        'font-transform-single-post-title' => 'none',

        // Page Title
        'font-family-page-title'              => 'Libre Franklin',
        'font-size-page-title'                => 50,
        'font-weight-page-title' => 'bold',
        'font-style-page-title' => 'normal',
        'font-transform-page-title' => 'none',

        /**
         * Color Scheme
         */
        // General
        'color-body-text'                     => '#444444',
        'color-logo'                          => '#222222',
        'color-logo-hover'                    => '#fb6640',
        'color-link'                          => '#fb6640',
        'color-link-hover'                    => '#111111',
        'color-hamburger'                     => '#222222',
        'color-hamburger-hover'               => '#fb6640',
        'color-search'                        => '#222222',
        'color-search-hover'                  => '#fb6640',
        // Menu
        'color-menu-link'                     => '#222222',
        'color-menu-link-hover'               => '#fb6640',
        'color-menu-link-current'             => '#fb6640',
        // Post
        'color-post-title'                    => '#222222',
        'color-post-title-hover'              => '#fb6640',
        'color-post-meta'                     => '#444444',
        'color-post-meta-link'                => '#fb6540',
        'color-post-meta-link-hover'          => '#222222',
        // Widgets
        'color-widget-title'                  => '#222222',
        'color-widget-title-footer'           => '#ffffff',
        // Footer
        'color-footer-background'             => '#222222',
        // Slider Background
        'color-slider-background'             => '#222222',

        /**
         * Single Post/Page
         */
        'featured-image-visible'              => 1,
        'featured-image-overlay-color'        => '#000000',
        'featured-image-overlay-opacity'      => 50,

        /**
         * Footer
         */
        // Widget Areas
        'footer-widget-areas'                 => 3,
        // Copyright
        'footer-text'                         => sprintf( __( 'Copyright &copy; %1$s &mdash; %2$s.', 'wpzoom' ), date( 'Y' ), get_bloginfo( 'name' ) ),
    );

    return $defaults;
}

function videobox_get_default( $option ) {
    $defaults = videobox_option_defaults();
    $default  = ( isset( $defaults[ $option ] ) ) ? $defaults[ $option ] : false;

    return $default;
}
