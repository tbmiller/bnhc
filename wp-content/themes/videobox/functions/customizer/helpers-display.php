<?php

function videobox_get_css_rules(){
    return array(
        'color-rules' => array(
            array(
                'id' => 'color-body-text',
                'selector' => 'body',
                'rule' => 'color'
            ),
            array(
                'id' => 'color-logo',
                'selector' => '.navbar-brand a',
                'rule' => 'color'
            ),
            array(
                'id' => 'color-logo-hover',
                'selector' => '.navbar-brand a:hover',
                'rule' => 'color'
            ),
            array(
                'id' => 'color-link',
                'selector' => 'a, .slides li .entry-meta a, .slides li .entry-meta a, .cat-links a',
                'rule' => 'color'
            ),
            array(
                'id' => 'color-link-hover',
                'selector' => 'a:hover, .slides li .entry-meta a:hover, .slides li .entry-meta a:hover, .cat-links a:hover, .single .post_author .author-title a:hover, .author_links a:hover, .author_links a:active, .comment-author .fn a:hover',
                'rule' => 'color'
            ),
            array(
                'id' => 'color-hamburger',
                'selector' => '.navbar-toggle .icon-bar',
                'rule' => 'background'
            ),
            array(
                'id' => 'color-hamburger-hover',
                'selector' => '.navbar-toggle:hover .icon-bar',
                'rule' => 'background'
            ),
            array(
                'id' => 'color-search',
                'selector' => '.sb-search .sb-search-submit',
                'rule' => 'color'
            ),
            array(
                'id' => 'color-search-hover',
                'selector' => '.sb-search .sb-search-submit:hover, .sb-search .sb-search-submit:focus, .sb-search .sb-search-input:focus',
                'rule' => 'color'
            ),

            // Main Menu
            array(
                'id' => 'color-menu-link',
                'selector' => '.main-menu a, #pageslide .panel .widget.widget_nav_menu ul.menu li a',
                'rule' => 'color'
            ),
            array(
                'id' => 'color-menu-link-hover',
                'selector' => '.main-menu a:hover, #pageslide .panel .widget.widget_nav_menu ul.menu li a:hover, #pageslide .panel .widget.widget_nav_menu ul.menu li a:active',
                'rule' => 'color'
            ),
            array(
                'id' => 'color-menu-link-current',
                'selector' => '.navbar-nav .current-menu-item a, .navbar-nav .current_page_item a, .navbar-nav .current-menu-parent a, .navbar-nav .current_page_parent a, #pageslide .panel .widget.widget_nav_menu ul.menu li.current-menu-item a',
                'rule' => 'color'
            ),
            // Post
            array(
                'id' => 'color-post-title',
                'selector' => '.entry-title a, .entry-title',
                'rule' => 'color'
            ),
            array(
                'id' => 'color-post-title-hover',
                'selector' => '.entry-title a:hover',
                'rule' => 'color'
            ),
            array(
                'id' => 'color-post-meta',
                'selector' => '.entry-meta',
                'rule' => 'color'
            ),
            array(
                'id' => 'color-post-meta-link',
                'selector' => '.entry-meta a',
                'rule' => 'color'
            ),
            array(
                'id' => 'color-post-meta-link-hover',
                'selector' => '.entry-meta a:hover',
                'rule' => 'color'
            ),
            // Widgets
            array(
                'id' => 'color-widget-title',
                'selector' => '#pageslide .panel .widget h3.title, .widget .title',
                'rule' => 'color'
            ),
            // Widgets in Footer
            array(
                'id' => 'color-widget-title-footer',
                'selector' => '.site-footer .widget h3.title, .site-footer .widget h3.title a',
                'rule' => 'color'
            ),
            // Footer
            array(
                'id' => 'color-footer-background',
                'selector' => '.site-footer',
                'rule' => 'background'
            ),
            // Slider
            array(
                'id' => 'color-slider-background',
                'selector' => '#slider',
                'rule' => 'background'
            )
        ),
        'font-rules' => array(
            //Body Font Styles
            array(
                'id' => 'font-weight-site-body',
                'selector' => 'body',
                'rule' => 'font-weight'
            ),
            array(
                'id' => 'font-weight-site-body',
                'selector' => 'body',
                'rule' => 'font-style'
            ),
            //Post Body Styles
            array(
                'id' => 'font-weight-site-paragraph',
                'selector' => 'body',
                'rule' => 'font-weight'
            ),
            array(
                'id' => 'font-weight-site-paragraph',
                'selector' => 'body',
                'rule' => 'font-style'
            ),
            //Site Title Font Styles
            array(
                'id' => 'font-weight-site-title',
                'selector' => '.navbar-brand h1 a, .navbar-brand h1',
                'rule' => 'font-weight'
            ),
            array(
                'id' => 'font-style-site-title',
                'selector' => '.navbar-brand h1 a, .navbar-brand h1',
                'rule' => 'font-style'
            ),
            array(
                'id' => 'font-transform-site-title',
                'selector' => '.navbar-brand h1 a, .navbar-brand h1',
                'rule' => 'text-transform'
            ),
            //Main Nav Menu Font Styles
            array(
                'id' => 'font-weight-nav',
                'selector' => '.main-menu a',
                'rule' => 'font-weight'
            ),
            array(
                'id' => 'font-style-nav',
                'selector' => '.main-menu a',
                'rule' => 'font-style'
            ),
            array(
                'id' => 'font-transform-nav',
                'selector' => '.main-menu a',
                'rule' => 'text-transform'
            ),
            // Slider Title Font Styles
            array(
                'id' => 'font-weight-slider-title',
                'selector' => '.slides li h3',
                'rule' => 'font-weight'
            ),
            array(
                'id' => 'font-style-slider-title',
                'selector' => '.slides li h3',
                'rule' => 'font-style'
            ),
            array(
                'id' => 'font-transform-slider-title',
                'selector' => '.slides li h3',
                'rule' => 'text-transform'
            ),
            // Widget Title Font Styles
            array(
                'id' =>'font-weight-widgets',
                'selector' => '.widget h3.title',
                'rule' => 'font-weight'
            ),
            array(
                'id' =>'font-style-widgets',
                'selector' => '.widget h3.title',
                'rule' => 'font-style'
            ),
            array(
                'id' =>'font-transform-widgets',
                'selector' => '.widget h3.title',
                'rule' => 'text-transform'
            ),
            // Post Title Font Styles
            array(
                'id' => 'font-weight-post-title',
                'selector' => '.entry-title a',
                'rule' => 'font-weight'
            ),
            array(
                'id' => 'font-style-post-title',
                'selector' => '.entry-title a',
                'rule' => 'font-style'
            ),
            array(
                'id' => 'font-transform-post-title',
                'selector' => '.entry-title a',
                'rule' => 'text-transform'
            ),
            //Single Post Title Font Styles
            array(
                'id' => 'font-weight-single-post-title',
                'selector' => '.single h1.entry-title',
                'rule' => 'font-weight'
            ),
            array(
                'id' => 'font-style-single-post-title',
                'selector' => '.single h1.entry-title',
                'rule' => 'font-style'
            ),
            array(
                'id' => 'font-transform-single-post-title',
                'selector' => '.single h1.entry-title',
                'rule' => 'text-transform'
            ),
            //Page Title
            array(
                'id' => 'font-weight-page-title',
                'selector' => '.page h1.entry-title',
                'rule' => 'font-weight'
            ),
            array(
                'id' => 'font-style-page-title',
                'selector' => '.page h1.entry-title',
                'rule' => 'font-style'
            ),
            array(
                'id' => 'font-transform-page-title',
                'selector' => '.page h1.entry-title',
                'rule' => 'text-transform'
            ),
        ),
        'font-extra-rules' => array(
            array(
                'id' => 'site-body',
                'selector' => 'body'
            ),
            array(
                'id' => 'site-paragraph',
                'selector' => '.entry-content'
            ),
            array(
                'id' => 'site-title',
                'selector' => '.navbar-brand h1, .navbar-brand h1 a'
            ),
            array(
                'id' => 'nav',
                'selector' => '.main-menu a'
            ),
            array(
                'id' => 'slider-title',
                'selector' => '.slides li h3, .slides li h3 a'
            ),
            array(
                'id' => 'post-title',
                'selector' => '.entry-title a'
            ),
            array(
                'id' => 'single-post-title',
                'selector' => '.single h1.entry-title'
            ),
            array(
                'id' => 'page-title',
                'selector' => '.page h1.entry-title'
            ),
            array(
                'id' => 'widgets',
                'selector' => '.widget h3.title'
            )
        )
    );
}

/**
 * Process user options to generate CSS needed to implement the choices.
 *
 * This function reads in the options from theme mods and determines whether a CSS rule is needed to implement an
 * option. CSS is only written for choices that are non-default in order to avoid adding unnecessary CSS. All options
 * are also filterable allowing for more precise control via a child theme or plugin.
 *
 * Note that all CSS for options is present in this function except for the CSS for fonts and the logo, which require
 * a lot more code to implement.
 *
 * @return void
 */
function videobox_css_add_rules() {

    $rules = videobox_get_css_rules();
    foreach($rules['color-rules'] as $color_rule) {
        videobox_css_add_simple_color_rule($color_rule['id'], $color_rule['selector'], $color_rule['rule']);
    }


    foreach($rules['font-rules'] as $font_rule) {
        videobox_css_add_simple_font_rule($font_rule['id'], $font_rule['selector'], $font_rule['rule']);
    }
}

add_action( 'videobox_css', 'videobox_css_add_rules' );

function videobox_css_add_simple_color_rule( $setting_id, $selectors, $declarations ) {
    $value = maybe_hash_hex_color( get_theme_mod( $setting_id, videobox_get_default( $setting_id ) ) );

    if ( $value === videobox_get_default( $setting_id ) ) {
        return;
    }

    if ( strtolower( $value ) === strtolower( videobox_get_default( $setting_id ) ) ) {
        return;
    }

    if ( is_string( $selectors ) ) {
        $selectors = array( $selectors );
    }

    if ( is_string( $declarations ) ) {
        $declarations = array(
            $declarations => $value
        );
    }

    videobox_get_css()->add( array(
        'selectors'    => $selectors,
        'declarations' => $declarations
    ) );
}



function videobox_css_add_simple_font_rule( $setting_id, $selectors, $declarations ) {
    $value =  get_theme_mod( $setting_id, videobox_get_default( $setting_id ) );

    if ( $value === videobox_get_default( $setting_id ) ) {
        return;
    }

    if ( strtolower( $value ) === strtolower( videobox_get_default( $setting_id ) ) ) {
        return;
    }

    if ( is_string( $selectors ) ) {
        $selectors = array( $selectors );
    }

    if ( is_string( $declarations ) ) {
        $declarations = array(
            $declarations => $value
        );
    }

    videobox_get_css()->add( array(
        'selectors'    => $selectors,
        'declarations' => $declarations
    ) );
}

