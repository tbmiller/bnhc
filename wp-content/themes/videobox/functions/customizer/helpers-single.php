<?php

if ( ! function_exists( 'videobox_css_single' ) ) :
    /**
     * Build the CSS rules for the custom styles on single posts/pages
     *
     * @return void
     */
    function videobox_css_single() {
        /**
         * Featured Image Overlay
         */
        $declarations = array();
        $overlay_color = maybe_hash_hex_color( get_theme_mod( 'featured-image-overlay-color', videobox_get_default( 'featured-image-overlay-color' ) ) );
        $overlay_opacity = max( 0, min( 100, absint( get_theme_mod( 'featured-image-overlay-opacity', videobox_get_default( 'featured-image-overlay-opacity' ) ) ) ) ) / 100;
        if ( !empty( $overlay_color ) ) $declarations['background-color'] = $overlay_color;
        if ( !empty( $overlay_opacity ) ) $declarations['opacity'] = $overlay_opacity;
        if ( ! empty( $declarations ) ) {
            videobox_get_css()->add( array( 'selectors' => array( '.entry-header .entry-cover-overlay' ), 'declarations' => $declarations ) );
        }
    }
endif;

add_action( 'videobox_css', 'videobox_css_single' );