<?php
/*-----------------------------------------------------------------------------------*/
/* Initializing Widgetized Areas (Sidebars)				 							 */
/*-----------------------------------------------------------------------------------*/

register_sidebar(array('name'=>'Sidebar',
   'id' => 'sidebar',
   'description' => 'Main sidebar that is displayed on the right and can be toggled by clicking on the hamburger icon from the main menu.',
   'before_widget' => '<div class="widget %2$s" id="%1$s">',
   'after_widget' => '<div class="clear"></div></div>',
   'before_title' => '<h3 class="title">',
   'after_title' => '</h3>',
));

register_sidebar(array('name'=>'Post/Page Sidebar',
   'id' => 'pp-sidebar',
   'description' => 'Sidebar that is displayed on single posts and pages.',
   'before_widget' => '<div class="widget %2$s" id="%1$s">',
   'after_widget' => '<div class="clear"></div></div>',
   'before_title' => '<h3 class="title">',
   'after_title' => '</h3>',
));

/* Mobile Sidebar
===============================*/

register_sidebar(array('name'=>'Homepage (Below slider)',
    'id' => 'homepage',
    'description' => 'Widget area for "WPZOOM: Carousel Slider" widget.',
    'before_widget' => '<div class="widget %2$s" id="%1$s">',
    'after_widget' => '<div class="clear"></div></div>',
    'before_title' => '<h3 class="title">',
    'after_title' => '</h3>',
));

register_sidebar(array('name'=>'Homepage (Above footer)',
    'id' => 'homepage-lower',
    'description' => 'Widget area above footer on homepage.',
    'before_widget' => '<div class="widget %2$s" id="%1$s">',
    'after_widget' => '<div class="clear"></div></div>',
    'before_title' => '<h3 class="title">',
    'after_title' => '</h3>',
));


register_sidebar(array('name'=>'Single Post (after content)',
   'id' => 'sidebar-post',
   'description' => 'Widget area that appears in individual posts after the content. Can be used for a Newsletter Form (Recommended plugin: MailPoet).',
   'before_widget' => '<div class="widget %2$s" id="%1$s">',
   'after_widget' => '<div class="clear"></div></div>',
   'before_title' => '<h3 class="title">',
   'after_title' => '</h3>',
));


/*----------------------------------*/
/* Footer widgetized areas		    */
/*----------------------------------*/

register_sidebar(array('name'=>'Footer (Full-width Area)',
    'description' => 'Widget area for "Carousel Slider" widget.',
    'id' => 'widgetized_section',
    'before_widget' => '<div class="widget %2$s" id="%1$s">',
    'after_widget' => '<div class="clear"></div></div>',
    'before_title' => '<h3 class="title">',
    'after_title' => '</h3>',
));




/*----------------------------------*/
/* Footer widgetized areas          */
/*----------------------------------*/

register_sidebar( array(
    'name'          => 'Footer: Column 1',
    'id'            => 'footer_1',
    'before_widget' => '<div class="widget %2$s" id="%1$s">',
    'after_widget'  => '<div class="clear"></div></div>',
    'before_title'  => '<h3 class="title">',
    'after_title'   => '</h3>',
) );

register_sidebar( array(
    'name'          => 'Footer: Column 2',
    'id'            => 'footer_2',
    'before_widget' => '<div class="widget %2$s" id="%1$s">',
    'after_widget'  => '<div class="clear"></div></div>',
    'before_title'  => '<h3 class="title">',
    'after_title'   => '</h3>',
) );

register_sidebar( array(
    'name'          => 'Footer: Column 3',
    'id'            => 'footer_3',
    'before_widget' => '<div class="widget %2$s" id="%1$s">',
    'after_widget'  => '<div class="clear"></div></div>',
    'before_title'  => '<h3 class="title">',
    'after_title'   => '</h3>',
) );
