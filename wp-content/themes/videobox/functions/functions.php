<?php
/**
 * Theme functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 */

if ( ! function_exists( 'videobox_setup' ) ) :
/**
 * Theme setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 */
function videobox_setup() {
	// This theme styles the visual editor to resemble the theme style.
	add_editor_style( array( 'css/editor-style.css' ) );

	/* Homepage Slider */

    add_image_size( 'loop', 384, 216, true );
    add_image_size( 'loop-portrait', 384, 569, true);
    add_image_size( 'loop-large', 600, 338, true );
	add_image_size( 'loop-large-portrait', 600, 888, true );
	add_image_size( 'entry-cover', 1800 );
	add_image_size( 'slider', 1500, 600, true);
	add_image_size( 'archive-header-background', 9999, 230, true);

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	// Register nav menus
	register_nav_menus( array(
		'primary' => __( 'Main Menu', 'wpzoom' )

	) );


	/*
	 * JetPack Infinite Scroll
	 */
	add_theme_support( 'infinite-scroll', array(
		'container' => 'recent-posts',
		'wrapper' => false,
		'footer' => false
	) );
}
endif;
add_action( 'after_setup_theme', 'videobox_setup' );



/*  Add support for Custom Background
==================================== */

add_theme_support( 'custom-background' );



/* Register Video Post Format
==================================== */

add_theme_support( 'post-formats', array( 'video', 'gallery' ) );



/* Video auto-thumbnail
==================================== */

if (is_admin()) {
	WPZOOM_Video_Thumb::init();
}


/*  Recommended Plugins
========================================== */

function zoom_register_theme_required_plugins_callback($plugins){

    $plugins =  array_merge(array(

        array(
            'name'         => 'Jetpack',
            'slug'         => 'jetpack',
            'required'     => true,
        ),

        array(
            'name'         => 'Instagram Widget by WPZOOM',
            'slug'         => 'instagram-widget-by-wpzoom',
            'required'     => false,
        )

    ), $plugins);

    return $plugins;
}

add_filter('zoom_register_theme_required_plugins', 'zoom_register_theme_required_plugins_callback');



/* Add special class to every post that has an embed code
============================================================ */

function category_id_class( $classes ) {
	global $post;

	if ( false !== ( $ec = get_post_meta( $post->ID, 'wpzoom_post_embed_code', true ) ) && trim( $ec ) != '' ) {
		$classes[] = 'wpz-has-embed-code';
	}

	return $classes;
}
add_filter( 'post_class', 'category_id_class' );



/* Video Embed Code Fix
==================================== */
if ( !function_exists( 'embed_fix' ) ) {
	function embed_fix($video,$width,$height) {

	  $video = preg_replace("/(width\s*=\s*[\"\'])[0-9]+([\"\'])/i", "$1 ".$width." $2", $video);
	  $video = preg_replace("/(height\s*=\s*[\"\'])[0-9]+([\"\'])/i", "$1 ".$height." $2", $video);
	  if (strpos($video, "<embed src=" ) !== false) {
		  $video = str_replace('</param><embed', '</param><param name="wmode" value="transparent"></param><embed wmode="transparent" ', $video);
	  }
	  else {
		if(strpos($video, "wmode=transparent") == false){

		  $re1='.*?'; # Non-greedy match on filler
		  $re2='((?:\\/{2}[\\w]+)(?:[\\/|\\.]?)(?:[^\\s"]*))';  # HTTP URL 1

		  if ($c=preg_match_all ("/".$re1.$re2."/is", $video, $matches))
		  {
			$httpurl1=$matches[1][0];
		  }

		  if(strpos($httpurl1, "?") == true){
			$httpurl_new = $httpurl1 . '&wmode=transparent';
		  }
		  else {
			$httpurl_new = $httpurl1 . '?wmode=transparent';
		  }

		  $search = array($httpurl1);
		  $replace = array($httpurl_new);
		  $video = str_replace($search, $replace, $video);

 		  unset($httpurl_new);

		}
	  }
	  return $video;
	}

}



/*  Add Support for Shortcodes in Excerpt
========================================== */

add_filter( 'the_excerpt', 'shortcode_unautop' );
add_filter( 'the_excerpt', 'do_shortcode' );

add_filter( 'widget_text', 'shortcode_unautop' );
add_filter( 'widget_text', 'do_shortcode' );


/*  Custom Excerpt Length
==================================== */

function new_excerpt_length( $length ) {
    return (int) option::get( "excerpt_length" ) ? (int)option::get( "excerpt_length" ) : 45;
}

add_filter( 'excerpt_length', 'new_excerpt_length' );



/*  Infinite Scroll
==================================== */

function videobox_infinite_scroll_js_settings( $settings ) {
	$settings['text'] = esc_js( esc_html( option::get( 'infinite_scroll_handle_text' ) ) );

	return $settings;
}
add_filter( 'infinite_scroll_js_settings', 'videobox_infinite_scroll_js_settings' );



/*  Maximum width for images in posts
=========================================== */

if ( ! isset( $content_width ) ) $content_width = 1200;



/*  Enqueue script for custom customize controls
=================================================== */

if ( ! function_exists( 'videobox_customize_enqueue' ) ) :
function videobox_customize_enqueue() {
	wp_enqueue_style( 'videobox-customize-css', get_template_directory_uri() . '/css/customizer.css' );
	wp_enqueue_script( 'videobox-customize-js', get_template_directory_uri() . '/js/customizer.js', array( 'jquery', 'customize-controls' ), false, true );
	wp_localize_script( 'videobox-customize-js', 'wpzoomCustomizerI10n', array( 'menu' => __( 'Menu', 'wpzoom' ), 'logo' => __( 'Logo', 'wpzoom' ), 'search' => __( 'Search', 'wpzoom' ) ) );
}
endif;
add_action( 'customize_controls_enqueue_scripts', 'videobox_customize_enqueue' );

if ( ! function_exists( 'videobox_customize_preview_js' ) ) :
function videobox_customize_preview_js() {
	wp_enqueue_script( 'videobox-customize-preview-js', get_template_directory_uri() . '/js/customizer-preview.js', array( 'customize-preview', 'jquery' ) );
}
endif;
add_action( 'customize_preview_init', 'videobox_customize_preview_js' );



if ( ! function_exists( 'videobox_get_the_archive_title' ) ) :
/* Custom Archives titles.
=================================== */
function videobox_get_the_archive_title( $title ) {
	if ( is_category() ) {
		global $cat;
		$category_color = esc_attr( preg_replace( '/[^0-9a-fA-F]/', '', trim( get_term_meta( $cat, 'wpzoom_category_color', true ) ) ) );
		$style = !empty( $category_color ) ? ' style="color:#' . $category_color . '"' : '';
		$title = sprintf( __( 'Category / <strong%s>%s</strong>', 'wpzoom' ), $style, single_cat_title( '', false ) );
	} elseif ( is_tag() ) {
		$title = sprintf( __( 'Tag / <strong>%s</strong>', 'wpzoom' ), single_tag_title( '', false ) );
	} elseif ( is_author() ) {
		$title = sprintf( __( 'Author / <strong>%s</strong>', 'wpzoom' ), '<span class="vcard">' . get_the_author() . '</span>' );
	} elseif ( is_year() ) {
		$title = sprintf( __( 'Year / <strong>%s</strong>', 'wpzoom' ), get_the_date( _x( 'Y', 'yearly archives date format', 'wpzoom' ) ) );
	} elseif ( is_month() ) {
		$title = sprintf( __( 'Month / <strong>%s</strong>', 'wpzoom' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'wpzoom' ) ) );
	} elseif ( is_day() ) {
		$title = sprintf( __( 'Day: <strong>%s</strong>', 'wpzoom' ), get_the_date( _x( 'F j, Y', 'daily archives date format', 'wpzoom' ) ) );
	} elseif ( is_tax( 'post_format' ) ) {
		if ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = _x( 'Asides', 'post format archive title', 'wpzoom' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = _x( 'Galleries', 'post format archive title', 'wpzoom' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = _x( 'Images', 'post format archive title', 'wpzoom' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = _x( 'Videos', 'post format archive title', 'wpzoom' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = _x( 'Quotes', 'post format archive title', 'wpzoom' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = _x( 'Links', 'post format archive title', 'wpzoom' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = _x( 'Statuses', 'post format archive title', 'wpzoom' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = _x( 'Audio', 'post format archive title', 'wpzoom' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = _x( 'Chats', 'post format archive title', 'wpzoom' );
		}
	} elseif ( is_post_type_archive() ) {
		$title = sprintf( __( 'Archives / <strong>%s</strong>', 'wpzoom' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
		$title = sprintf( __( '%1$s / <strong>%2$s</strong>', 'wpzoom' ), $tax->labels->singular_name, single_term_title( '', false ) );
	} else {
		$title = __( 'Archives', 'wpzoom' );
	}

	return $title;
}
endif;
add_filter( 'get_the_archive_title', 'videobox_get_the_archive_title' );



if ( ! function_exists( 'videobox_alter_main_query' ) ) :
/**
 * Alter main WordPress Query to exclude specific categories
 * and posts from featured slider if this is configured via Theme Options.
 *
 * @param $query WP_Query
 */
function videobox_alter_main_query( $query ) {
	// until this is fixed https://core.trac.wordpress.org/ticket/27015
	$is_front = false;

	if ( get_option( 'page_on_front' ) == 0 ) {
		$is_front = is_front_page();
	} else {
		$is_front = $query->get( 'page_id' ) == get_option( 'page_on_front' );
	}

	if ( $query->is_main_query() && $is_front ) {
		if ( option::is_on( 'hide_featured' ) ) {
			$featured_posts = new WP_Query( array(
				'post__not_in'   => get_option( 'sticky_posts' ),
				'posts_per_page' => option::get( 'slideshow_posts' ),
				'meta_key'       => 'wpzoom_is_featured',
				'meta_value'     => 1
			) );

			$postIDs = array();
			while ( $featured_posts->have_posts() ) {
				$featured_posts->the_post();
				$postIDs[] = get_the_ID();
			}

			wp_reset_postdata();

			$query->set( 'post__not_in', $postIDs );
		}

		if ( count( option::get( 'recent_part_exclude' ) ) ) {
			$query->set( 'cat', '-' . implode( ',-', (array) option::get('recent_part_exclude') ) );
		}

		if ( isset( $_GET['sort'] ) && $_GET['sort'] == 'popular' ) {
            if ( option::get( 'popular_posts_method' ) == 'Number of Comments' ) {
            	$query->set( 'orderby', 'comment_count' );
            } else {
	            $query->set( 'meta_key', 'Views' );
	            $query->set( 'orderby', 'meta_value_num' );
				$query->set( 'order', 'DESC' );
			}
			$query->set( 'ignore_sticky_posts', true );

			switch ( trim( option::get( 'popular_posts_time_period' ) ) ) {
				case 'This Year':
					$query->set( 'year', date('Y') );
					break;

				case 'This Month':
					$query->set( 'year', date('Y') );
					$query->set( 'month', date('n') );
					break;

				case 'This Week':
					$query->set( 'year', date('Y') );
					$query->set( 'month', date('n') );
					$query->set( 'week', date('W') );
					break;
			}
		}
	}
}
endif;
add_action( 'pre_get_posts', 'videobox_alter_main_query' );




/* Register Custom Fields in Profile: Facebook, Twitter
===================================================== */

add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

function my_show_extra_profile_fields( $user ) { ?>

	<h3>Additional Profile Information</h3>

	<table class="form-table">


		<tr>
			<th><label for="twitter">Twitter Username</label></th>

			<td>
				<input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Twitter username</span>
			</td>
		</tr>

		<tr>
			<th><label for="facebook_url">Facebook Profile URL</label></th>

			<td>
				<input type="text" name="facebook_url" id="facebook_url" value="<?php echo esc_attr( get_the_author_meta( 'facebook_url', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Facebook profile URL</span>
			</td>
		</tr>

		<tr>
			<th><label for="facebook_url">Instagram Username</label></th>

			<td>
				<input type="text" name="instagram_url" id="instagram_url" value="<?php echo esc_attr( get_the_author_meta( 'instagram_url', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Instagram username</span>
			</td>
		</tr>

	</table>
<?php }

add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	update_user_meta( $user_id, 'instagram_url', $_POST['instagram_url'] );
	update_user_meta( $user_id, 'facebook_url', $_POST['facebook_url'] );
	update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
}



/* Add color field to category add/edit pages
============================================== */

function wpzoom_category_color_field_add() {
	?><div class="form-field wpzoom-category-color">
		<label for="wpzoom_category_color"><?php _e( 'Color', 'wpzoom' ); ?></label>
		<input type="text" name="wpzoom_category_color" id="wpzoom_category_color" value="#fb6540" size="7" aria-required="true" />
		<p class="description"><?php _e( 'The color of the category. The text label of the category will take this color throughout the theme.', 'wpzoom' ); ?></p>
	</div><?php
}
add_action( 'category_add_form_fields', 'wpzoom_category_color_field_add', 10 );

function wpzoom_category_color_field_edit( $tag, $taxonomy ) {
	$category_color = esc_attr( preg_replace( '/[^0-9a-fA-F]/', '', trim( get_term_meta( $tag->term_id, 'wpzoom_category_color', true ) ) ) );

	?><tr class="form-field wpzoom-category-color">
		<th scope="row" valign="top"><label for="wpzoom_category_color"><?php _e( 'Color', 'wpzoom' ); ?></label></th>
		<td>
			<input type="text" name="wpzoom_category_color" id="wpzoom_category_color" value="#<?php echo !empty( $category_color ) ? $category_color : 'fb6540'; ?>" size="7" aria-required="true" />
			<p class="description"><?php _e( 'The color of the category. The text label of the category will take this color throughout the theme.', 'wpzoom' ); ?></p>
		</td>
	</tr><?php
}
add_action( 'category_edit_form_fields', 'wpzoom_category_color_field_edit', 10, 2 );

function wpzoom_category_color_field_save( $term_id, $tt_id ) {
	if ( isset( $_POST['wpzoom_category_color'] ) ) {
		update_term_meta( $term_id, 'wpzoom_category_color', '#' . trim( preg_replace( '/[^0-9a-fA-F]/', '', $_POST['wpzoom_category_color'] ) ) );
	}
}
add_action( 'created_category', 'wpzoom_category_color_field_save', 10, 2 );
add_action( 'edited_category', 'wpzoom_category_color_field_save', 10, 2 );

function wpzoom_category_color_admin_enqueue_scripts( $hook ) {
	if ( $hook != 'edit-tags.php' && $hook != 'term.php' ) return;

	wp_enqueue_script( 'wp-color-picker' );
	wp_enqueue_style( 'wp-color-picker' );
}
add_action( 'admin_enqueue_scripts', 'wpzoom_category_color_admin_enqueue_scripts' );

function wpzoom_category_color_admin_head_edit_tags() {
	?><style type="text/css">
		.column-wpzoom-color {
			word-wrap: normal;
			width: 3em;
		}

		.wpzoom-column-category-color {
			display: inline-block;
			height: 25px;
			width: 25px;
			border-radius: 5px;
			box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.3);
		}
	</style><?php
}
add_action( 'admin_head-edit-tags.php', 'wpzoom_category_color_admin_head_edit_tags' );
add_action( 'admin_head-term.php', 'wpzoom_category_color_admin_head_edit_tags' );

function wpzoom_category_color_admin_head() {
	?><style type="text/css">
		<?php
		$terms = get_terms( 'category', array( 'hide_empty=0' ) );

		if ( !empty( $terms ) && !is_wp_error( $terms ) ) {
			foreach ( $terms as $term ) {
				$category_color = esc_attr( preg_replace( '/[^0-9a-fA-F]/', '', trim( get_term_meta( $term->term_id, 'wpzoom_category_color', true ) ) ) );
				if ( !empty( $category_color ) ) echo 'a[href$="edit.php?category_name=' . $term->slug . '"] { color: #' . $category_color . "; }\n";
			}
		}
		?>
	</style><?php
}
add_action( 'admin_head', 'wpzoom_category_color_admin_head' );

function wpzoom_category_color_admin_footer() {
	?><script type="text/javascript">
		jQuery(function($){
			$('#wpzoom_category_color').wpColorPicker({
				palettes: ['#fb6540', '#2db855', '#a085cc', '#a3af2f', '#40d6fb', '#dfb44c', '#fb40b6']
			});
		});
	</script><?php
}
add_action( 'admin_footer-edit-tags.php', 'wpzoom_category_color_admin_footer' );
add_action( 'admin_footer-term.php', 'wpzoom_category_color_admin_footer' );

function wpzoom_category_color_manage_edit_category_columns( $columns ) {
	return array_slice( $columns, 0, 2, true ) + array( 'wpzoom-color' => __( 'Color', 'wpzoom' ) ) + array_slice( $columns, 2, count($columns) - 1, true );
}
add_filter( 'manage_edit-category_columns', 'wpzoom_category_color_manage_edit_category_columns', 10 );

function wpzoom_category_color_manage_category_custom_fields( $deprecated, $column_name, $term_id ) {
	if ( $column_name == 'wpzoom-color' ) {
		$category_color = esc_attr( preg_replace( '/[^0-9a-fA-F]/', '', trim( get_term_meta( $term_id, 'wpzoom_category_color', true ) ) ) );
		echo '<span class="wpzoom-column-category-color" style="background-color:#' . ( !empty( $category_color ) ? $category_color : 'fb6540' ) . '"></span>';
	}
}
add_filter( 'manage_category_custom_column', 'wpzoom_category_color_manage_category_custom_fields', 10, 3 );



/* Filter the output of the_category to include colors
======================================================== */

if ( !is_admin() ) {
	function wpzoom_get_the_category_list( $separator = '', $parents='', $post_id = false ) {
		global $wp_rewrite;

		if ( ! is_object_in_taxonomy( get_post_type( $post_id ), 'category' ) ) {
			return '';
		}

		$categories = apply_filters( 'the_category_list', get_the_category( $post_id ), $post_id );

		if ( empty( $categories ) ) {
			return __( 'Uncategorized', 'wpzoom' );
		}

		$rel = ( is_object( $wp_rewrite ) && $wp_rewrite->using_permalinks() ) ? 'rel="category tag"' : 'rel="category"';

		$thelist = '';
		if ( '' == $separator ) {
			$thelist .= '<ul class="post-categories">';
			foreach ( $categories as $category ) {
				$color = esc_attr( preg_replace( '/[^0-9a-fA-F]/', '', trim( get_term_meta( $category->term_id, 'wpzoom_category_color', true ) ) ) );
				$color_style = !empty( $color ) ? ' style="color:#' . $color . '"' : '';
				$thelist .= "\n\t<li>";
				switch ( strtolower( $parents ) ) {
					case 'multiple':
						if ( $category->parent )
							$thelist .= get_category_parents( $category->parent, true, $separator );
						$thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . $color_style . '>' . $category->name.'</a></li>';
						break;
					case 'single':
						$thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '"  ' . $rel . $color_style . '>';
						if ( $category->parent )
							$thelist .= get_category_parents( $category->parent, false, $separator );
						$thelist .= $category->name.'</a></li>';
						break;
					case '':
					default:
						$thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . $color_style . '>' . $category->name.'</a></li>';
				}
			}
			$thelist .= '</ul>';
		} else {
			$i = 0;
			foreach ( $categories as $category ) {
				$color = esc_attr( preg_replace( '/[^0-9a-fA-F]/', '', trim( get_term_meta( $category->term_id, 'wpzoom_category_color', true ) ) ) );
				$color_style = !empty( $color ) ? ' style="color:#' . $color . '"' : '';

				if ( 0 < $i )
					$thelist .= $separator;
				switch ( strtolower( $parents ) ) {
					case 'multiple':
						if ( $category->parent )
							$thelist .= get_category_parents( $category->parent, true, $separator );
						$thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . $color_style . '>' . $category->name.'</a>';
						break;
					case 'single':
						$thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . $color_style . '>';
						if ( $category->parent )
							$thelist .= get_category_parents( $category->parent, false, $separator );
						$thelist .= "$category->name</a>";
						break;
					case '':
					default:
						$thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . $color_style . '>' . $category->name.'</a>';
				}
				++$i;
			}
		}

		return $thelist;
	}

	function wpzoom_filter_the_category( $thelist, $separator, $parents ) {
		return wpzoom_get_the_category_list( $separator, $parents );
	}
	add_filter( 'the_category', 'wpzoom_filter_the_category', 10, 3 );
}



/* Category Image Covers
==================================== */

function wpz_category_form_scripts( $hook ) {
	if ( $hook != 'edit-tags.php' && $hook != 'term.php' ) return;
	wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'wpz_category_form_scripts' );

function wpz_category_form_head() {
	?><style type="text/css">
		#wpz_category_cover_image_preview { display: block; position: relative; background: #222 center no-repeat; background-size: contain; min-height: 300px; width: 95%; margin: 2px 0 5px }
		#wpz_category_cover_image_preview.has-image #wpz_category_cover_image_btnwrap { opacity: 0; -moz-transition: opacity 0.4s; -webkit-transition: opacity 0.4s; transition: opacity 0.4s }
		#wpz_category_cover_image_preview.has-image:hover #wpz_category_cover_image_btnwrap { opacity: 1 }
		#wpz_category_cover_image_btnwrap { position: absolute; top: 50%; left: 50%; background: rgba(0, 0, 0, 0.7); padding: 5px; -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px; -moz-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); transform: translate(-50%, -50%) }
		#wpz_category_cover_image_clrbtn.disabled { opacity: 0.5 }
		#wpz_category_cover_image_pos_label { opacity: 0.4; -moz-transition: opacity 0.4s; -webkit-transition: opacity 0.4s; transition: opacity 0.4s }
		#wpz_category_cover_image_pos_label:hover { opacity: 1 }
		#wpz_category_cover_image_pos_label > small { display: inline; color: #999 }
		#wpz_category_cover_image_pos { width: auto }
	</style>
	<script type="text/javascript">
		jQuery(function($){
			var frame;

			$('#wpz_category_cover_image_btnwrap')
				.on('click', '#wpz_category_cover_image_selbtn', function(e){
					e.preventDefault();

					var $el = $(this);

					if ( frame ) {
						frame.open();
						return;
					}

					frame = wp.media({
						title: '<?php _e( 'Select Image', 'wpzoom' ); ?>',
						library: { type: 'image' },
						multiple: false,
						button: { text: '<?php _e( 'Choose Image', 'wpzoom' ); ?>' }
					});

					frame.on('open', function(){
						if ( $.trim( $('#wpz_category_cover_image_id').val() ) == '' ) return;
						attachment = wp.media.attachment( parseInt($('#wpz_category_cover_image_id').val(), 10) );
						attachment.fetch();
						frame.state().get('selection').add( attachment ? [ attachment ] : [] );
					});

					frame.on('select', function(){
						var imgdata = frame.state().get('selection').first().attributes;
						$('#wpz_category_cover_image_id').val(parseInt(imgdata.id, 10));
						$('#wpz_category_cover_image_preview').css('background-image', 'url("' + imgdata.url + '")');

						var maxWidth = $('#wpz_category_cover_image_preview').width();
						var width = parseInt(imgdata.width, 10);
						$('#wpz_category_cover_image_preview').css("height", ( parseInt(imgdata.height, 10) * ( maxWidth / width ) ) );

						$('#wpz_category_cover_image_preview:not(.has-image)').addClass('has-image');
						$('#wpz_category_cover_image_clrbtn.disabled').removeClass('disabled');
					});

					frame.open();
				})
				.on('click', '#wpz_category_cover_image_clrbtn:not(.disabled)', function(e){
					e.preventDefault();
					if ( frame ) frame.state().get('selection').reset();
					$('#wpz_category_cover_image_id').val('');
					$('#wpz_category_cover_image_preview').attr('style', '');
					$('#wpz_category_cover_image_preview.has-image').removeClass('has-image');
					$(this).addClass('disabled');
				});
		});
	</script><?php
}
add_action( 'admin_head-edit-tags.php', 'wpz_category_form_head' );
add_action( 'admin_head-term.php', 'wpz_category_form_head' );

function wpz_category_term_form_tag() {
	echo ' enctype="multipart/form-data" encoding="multipart/form-data"';
}
add_action( 'category_term_new_form_tag', 'wpz_category_term_form_tag' );
add_action( 'category_term_edit_form_tag', 'wpz_category_term_form_tag' );

function wpz_category_add_form_fields() {
	?><div class="form-field term-image-wrap">
		<label for="wpz_category_cover_image_selbtn"><?php _e( 'Cover Image', 'wpzoom' ); ?></label>
		<input name="wpz_category_cover_image_id" id="wpz_category_cover_image_id" type="hidden" value="" />
		<span id="wpz_category_cover_image_preview">
			<span id="wpz_category_cover_image_btnwrap">
				<input type="button" id="wpz_category_cover_image_selbtn" class="button" value="<?php _e( 'Choose Image', 'wpzoom' ); ?>" />
				<input type="button" id="wpz_category_cover_image_clrbtn" class="button disabled" value="<?php _e( 'Clear Image', 'wpzoom' ); ?>" />
			</span>
		</span>
		<p><?php _e( 'A cover image that represents the category.', 'wpzoom' ); ?></p>
	</div><?php
}
add_action( 'category_add_form_fields', 'wpz_category_add_form_fields' );

function wpz_category_edit_form_fields( $tag, $taxonomy ) {
	$term_meta = get_term_meta( $tag->term_id, 'wpz_cover_image_id', true );
	$image_id = $term_meta !== false ? absint( $term_meta ) : 0;
	if ( $image_id > 0 ) {
		$attachment = wp_get_attachment_image_src( $image_id, 'full' );
		if ( $attachment !== false && is_array( $attachment ) && count( $attachment ) > 2 ) {
			$image_url = $attachment[0];
			$image_height = $attachment[2];
		} else {
			$image_url = $image_height = '';
		}
	}
	$term_pos = get_term_meta( $tag->term_id, 'wpz_cover_image_pos', true );
	$imgpos = $term_pos !== false ? trim( $term_pos ) : '';

	?><tr class="form-field term-image-wrap">
		<th scope="row"><label for="wpz_category_cover_image_selbtn"><?php _e( 'Cover Image', 'wpzoom' ); ?></label></th>
		<td>
			<input name="wpz_category_cover_image_id" id="wpz_category_cover_image_id" type="hidden" value="<?php echo $image_id > 0 ? $image_id : ''; ?>" />
			<span id="wpz_category_cover_image_preview"<?php if ( $image_id > 0 ) echo ' class="has-image" style="background-image:url(\'' . $image_url . '\');height:' . $image_height . 'px"'; ?>>
				<span id="wpz_category_cover_image_btnwrap">
					<input type="button" id="wpz_category_cover_image_selbtn" class="button" value="<?php _e( 'Choose Image', 'wpzoom' ); ?>" />
					<input type="button" id="wpz_category_cover_image_clrbtn" class="button<?php echo $image_id > 0 ? '' : ' disabled'; ?>" value="<?php _e( 'Clear Image', 'wpzoom' ); ?>" />
				</span>
			</span>
			<label id="wpz_category_cover_image_pos_label"><strong><?php _e( 'Position:', 'wpzoom' ); ?></strong> <input type="text" name="wpz_category_cover_image_pos" id="wpz_category_cover_image_pos" value="<?php echo !empty( $imgpos ) ? esc_attr( $imgpos ) : 'center'; ?>" /> <small class="howto"><em><?php printf( __( 'See <a href="%s">here</a> for valid values.', 'wpzoom' ), esc_url( 'https://developer.mozilla.org/docs/Web/CSS/background-position' ) ); ?></em></small></label>
			<p class="description"><?php _e( 'A cover image that represents the category.', 'wpzoom' ); ?></p>
		</td>
	</tr><?php
}
add_action( 'category_edit_form_fields', 'wpz_category_edit_form_fields', 10, 2 );

function category_form_custom_field_save( $term_id, $tt_id ) {
	if ( isset( $_POST['wpz_category_cover_image_id'] ) ) {
		update_term_meta( $term_id, 'wpz_cover_image_id', ( !empty( $_POST['wpz_category_cover_image_id'] ) ? absint( $_POST['wpz_category_cover_image_id'] ) : '' ) );
	}

	if ( isset( $_POST['wpz_category_cover_image_pos'] ) ) {
		update_term_meta( $term_id, 'wpz_cover_image_pos', ( !empty( $_POST['wpz_category_cover_image_pos'] ) ? trim( $_POST['wpz_category_cover_image_pos'] ) : '' ) );
	}
}
add_action( 'created_category', 'category_form_custom_field_save', 10, 2 );
add_action( 'edited_category', 'category_form_custom_field_save', 10, 2 );








/* Count post views
==================================== */

add_action( 'template_redirect', 'entry_views_load' );
add_action( 'wp_ajax_entry_views', 'entry_views_update_ajax' );
add_action( 'wp_ajax_nopriv_entry_views', 'entry_views_update_ajax' );

function entry_views_load() {
  global $wp_query, $entry_views_pid;

  if ( is_singular() ) {

    $post = $wp_query->get_queried_object();
        $entry_views_pid = $post->ID;
        wp_enqueue_script( 'jquery' );
        add_action( 'wp_footer', 'entry_views_load_scripts' );
  }
}

function entry_views_update( $post_id = '' ) {
  global $wp_query;

  if ( !empty( $post_id ) ) {

    $meta_key = apply_filters( 'entry_views_meta_key', 'Views' );
    $old_views = get_post_meta( $post_id, $meta_key, true );
    $new_views = absint( $old_views ) + 1;
    update_post_meta( $post_id, $meta_key, $new_views, $old_views );
  }
}


function entry_views_get( $attr = '' ) {
  global $post;

  $attr = shortcode_atts( array( 'before' => '', 'after' => '', 'post_id' => $post->ID ), $attr );
  $meta_key = apply_filters( 'entry_views_meta_key', 'Views' );
  $views = intval( get_post_meta( $attr['post_id'], $meta_key, true ) );
  return $attr['before'] . number_format_i18n( $views ) . $attr['after'];
}


function entry_views_update_ajax() {

  check_ajax_referer( 'entry_views_ajax' );

  if ( isset( $_POST['post_id'] ) )
    $post_id = absint( $_POST['post_id'] );

  if ( !empty( $post_id ) )
    entry_views_update( $post_id );
}


function entry_views_load_scripts() {
  global $entry_views_pid;

  $nonce = wp_create_nonce( 'entry_views_ajax' );

  echo '<script type="text/javascript">/* <![CDATA[ */ jQuery(document).ready( function() { jQuery.post( "' . admin_url( 'admin-ajax.php' ) . '", { action : "entry_views", _ajax_nonce : "' . $nonce . '", post_id : ' . $entry_views_pid . ' } ); } ); /* ]]> */</script>' . "\n";
}



/* Allow to submit <iframe> embed code via Gravity Forms
==================================== */

add_filter( 'gform_sanitize_entry_value', '__return_false' );




/* Enqueue scripts and styles for the front end.
=========================================== */

function videobox_scripts() {
	if ( '' !== $google_request = videobox_get_google_font_uri() ) {
		wp_enqueue_style( 'videobox-google-fonts', $google_request, WPZOOM::$themeVersion );
	}

	// Load our main stylesheet.
	wp_enqueue_style( 'videobox-style', get_stylesheet_uri() );

	wp_enqueue_style( 'media-queries', get_template_directory_uri() . '/css/media-queries.css', array(), WPZOOM::$themeVersion );

	wp_enqueue_style( 'videobox-google-font-default', '//fonts.googleapis.com/css?family=Libre+Franklin:400,400i,600,600i,700,700i&subset=latin-ext' );

	wp_enqueue_style( 'dashicons' );

	wp_enqueue_script( 'flickity', get_template_directory_uri() . '/js/flickity.pkgd.min.js', array(), WPZOOM::$themeVersion, true );

	wp_enqueue_script( 'fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ), WPZOOM::$themeVersion, true );

	wp_enqueue_script( 'superfish', get_template_directory_uri() . '/js/superfish.min.js', array( 'jquery' ), WPZOOM::$themeVersion, true );

	$themeJsOptions = option::getJsOptions();

	wp_enqueue_script( 'videobox-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery', 'jquery-effects-slide' ), WPZOOM::$themeVersion, true );
	wp_localize_script( 'videobox-script', 'zoomOptions', $themeJsOptions );
}

add_action( 'wp_enqueue_scripts', 'videobox_scripts' );