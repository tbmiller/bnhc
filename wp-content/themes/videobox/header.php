<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div class="page-wrap">

	<?php if ( is_active_sidebar( 'sidebar' ) ) { /* Display the sidebar panel only if there are any widgets added in the sidebar */ ?>

		<div id="pageslide"<?php if ( get_theme_mod( 'menu-slide-dir', videobox_get_default( 'menu-slide-dir' ) ) == 2 ) echo ' class="slide-from-right"' ?>>
			<div id="slideNav" class="panel">

				<a href="#" class="closeBtn"></a>

				<?php get_sidebar(); ?>
			</div>
		</div>

	<?php } ?>

	<header class="site-header">

		<div class="inner-wrap">

			<?php
			$logo_visible = !get_theme_mod( 'logo-visible', videobox_get_default( 'logo-visible' ) ) ? 'hidden' : '';
			$menu_visible = !get_theme_mod( 'menu-visible', videobox_get_default( 'menu-visible' ) ) ? 'hidden' : '';
			$search_visible = !get_theme_mod( 'search-visible', videobox_get_default( 'search-visible' ) ) ? 'hidden' : '';
			$menu_type = get_theme_mod( 'menu-type', videobox_get_default( 'menu-type' ) );
			$order = array_flip( str_split( trim( get_theme_mod( 'header-elements-order', videobox_get_default( 'header-elements-order' ) ) ) ) );
			?>

			<div class="side-panel-btn <?php echo $menu_type != 1 ? 'hidden' : $menu_visible; ?> order-<?php echo $order['M']+1; ?>">

				<?php if ( is_active_sidebar( 'sidebar' ) ) { /* This is the button for displaying the sidebar. Hidden if there are no widgets in the Sidebar */ ?>

					<a class="navbar-toggle" href="#slideNav">
						<span class="sr-only"><?php _e( 'Toggle sidebar &amp; navigation', 'wpzoom' ); ?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>

				<?php } ?>

			</div><!-- .side-panel-btn -->

			<div class="main-menu <?php echo $menu_type != 2 ? 'hidden' : $menu_visible; ?> order-<?php echo $order['M']+1; ?>">

				<?php if ( has_nav_menu( 'primary' ) ) {
					wp_nav_menu( array(
						'container_id'   => 'menu-main-wrap',
						'menu_class'     => 'nav navbar-nav dropdown sf-menu',
						'theme_location' => 'primary'
					) );
				} ?>

			</div><!-- .main-menu -->

			<div class="navbar-brand <?php echo $logo_visible; ?> order-<?php echo $order['L']+1; ?>">
				<?php if ( ! videobox_has_logo() ) echo '<h1>'; ?>

				<a href="<?php echo home_url(); ?>" title="<?php bloginfo( 'description' ); ?>">

					<?php
					if ( videobox_has_logo() ) {
						videobox_logo();
					} else {
						bloginfo( 'name' );
					}
					?>

				</a>

				<?php if ( ! videobox_has_logo() ) echo '</h1>'; ?>

			</div><!-- .navbar-brand -->

			<div class="search-btn <?php echo $search_visible; ?> order-<?php echo $order['S']+1; ?>">

				<div id="sb-search" class="sb-search">
					<?php get_search_form(); ?>
				</div>

			</div><!-- .search-btn -->

		</div>

	</header><!-- .site-header -->