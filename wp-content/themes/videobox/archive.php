<?php
get_header();

$withbg = false;
$imgurl = $imgpos = '';
if ( is_category() && option::is_on( 'archive_header_background' ) ) {
	global $cat;
	$term_meta = get_term_meta( $cat, 'wpz_cover_image_id', true );
	$image_id = $term_meta !== false ? absint( $term_meta ) : 0;
	if ( $image_id > 0 ) {
		$attachment = wp_get_attachment_image_src( $image_id, 'entry-cover' );
		if ( $attachment !== false && is_array( $attachment ) && count( $attachment ) > 2 ) {
			$imgurl = $attachment[0];
			$term_pos = get_term_meta( $cat, 'wpz_cover_image_pos', true );
			$imgpos = $term_pos !== false ? 'background-position:' . esc_attr( trim( $term_pos ) ) . ';' : '';
			$withbg = true;
		}
	}
}
?>

<div class="header-archive<?php echo $withbg ? ' withbg' : ''; ?>"<?php echo $withbg ? ' style="background-image:url(\'' . $imgurl . '\');' . $imgpos . '"' : ''; ?>>

	<div class="inner-wrap">

		<div class="header-archive-wrap">

			<?php the_archive_title( '<h2 class="section-title">', '</h2>' ); ?>

			<?php if (is_category() ) { echo category_description(); } ?>

		</div>

	</div>

</div>

<div class="inner-wrap">

	<main id="main" class="site-main" role="main">

		<section class="recent-posts" id="recent-posts">

			<?php if ( have_posts() ) : ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', get_post_format() ); ?>

				<?php endwhile; ?>

				<?php get_template_part( 'pagination' ); ?>

			<?php else: ?>

				<?php get_template_part( 'content', 'none' ); ?>

			<?php endif; ?>

		</section><!-- .recent-posts -->

	</main><!-- .site-main -->

</div><!-- /.inner-wrap -->
<?php
get_footer();
