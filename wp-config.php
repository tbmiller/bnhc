<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bnhc_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's8Nnh>)@E3Zkz@5~8:KN%,Q~[At)3rA1T1[j[ +F$3Zyb>*T9k]}h(m{ePii!/f.');
define('SECURE_AUTH_KEY',  'lD&jxqH.y%<5OAuB04V#;-~0c0_1Iv54wosqm.got+hFGJM[}?V^/2+L.u6w.Vb,');
define('LOGGED_IN_KEY',    '};0bN+B^OiJ;eFNciAQZxa|=Ut].auM,lH[.@9-Qd&Q|vMtUMJp<yjKDLBA9p=dE');
define('NONCE_KEY',        'Pi==K9np~mG5p@[+6r;l]- ^aqG^a&6*H4e3X#4R!6+FfIE@}0$+&WokuYF3[Mw~');
define('AUTH_SALT',        's/PVEcg-^`r%.NQ;59px/<%FsVvid2srO1{ELo+RZ!+0tE8{`6j~RM4l~+1NH&O5');
define('SECURE_AUTH_SALT', '8)oCe@&ra-CNthP| ,ck|@[+N*`Xx~<]y|yAylCF9( HIF$uzrl$bhTuJ9icx-H]');
define('LOGGED_IN_SALT',   '($KlVH4uE^cLw>uRzrq$6q4`z/Nu+]iUE{ITJ{UX!.OH7R{v7,}!O &:drcsFiT9');
define('NONCE_SALT',       'y(!{6moXK+>?b!lo0#gK7n6%Taz3x2:v+pm j?*]nGT|$]7}z+cZg5Z:+0$.rr}4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
